/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file measure.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/12
 * @license
 * @description
 */


#include <stdio.h>
#include <string.h>
#include "measure.h"

#include "adc.h"
#include "opa.h"
#include "vc.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
#define MEASURE_ITEM_NUM            (AIP_OPCODE_TEST_END - AIP_OPCODE_TEST_AIN_00)
//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct measure_report
{
    uint16_t    err_code;
    uint16_t    value;
} measure_report_t;


//=============================================================================
//                  Global Data Definition
//=============================================================================
static measure_report_t     g_report[MEASURE_ITEM_NUM];
//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
measure_init(void)
{
    measure_err_t       rval = MEASURE_ERR_OK;
    memset(g_report, 0x0, sizeof(g_report));

    return rval;
}

measure_err_t
measure_proc(ad_package_t *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;
    aip_opcode_t        opcode = (aip_opcode_t)pPackage->base.opcode;

    if( !pPackage )
        return MEASURE_ERR_NULL_POINTER;

    msg("-> opcode x%X\n", opcode);

    switch( opcode )
    {
        default:
            rval = MEASURE_ERR_UNKNOWN_OPCODE;
            break;

#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_OPA1_OPEN:
            rval = opa_init(MEASURE_DEV_ID_OPA_1, opcode, pPackage);
            break;
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC:
        case AIP_OPCODE_TEST_OPA1_CALIBRATION:
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA1_INVERTING:
        case AIP_OPCODE_TEST_OPA1_NONINVERTING:
        case AIP_OPCODE_TEST_OPA1_DIFF:
            rval = opa_start(MEASURE_DEV_ID_OPA_1, opcode, pPackage);
            break;

        case AIP_OPCODE_TEST_OPA_CASCADING:
            rval = opa_init(MEASURE_DEV_ID_OPA_ALL, opcode, pPackage);
            if( rval )      break;

            rval = opa_start(MEASURE_DEV_ID_OPA_ALL, opcode, pPackage);
            if( rval )      break;
            break;

        case AIP_OPCODE_TEST_OPA2_OPEN:
            rval = opa_init(MEASURE_DEV_ID_OPA_2, opcode, pPackage);
            break;

        case AIP_OPCODE_TEST_OPA2_CALIBRATION:
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA2_INVERTING:
        case AIP_OPCODE_TEST_OPA2_NONINVERTING:
        case AIP_OPCODE_TEST_OPA2_DIFF:
            rval = opa_start(MEASURE_DEV_ID_OPA_2, opcode, pPackage);
            break;

        case AIP_OPCODE_TEST_OPA1_CLOSE:
            rval = opa_stop(MEASURE_DEV_ID_OPA_1, opcode);
            break;
        case AIP_OPCODE_TEST_OPA2_CLOSE:
            rval = opa_stop(MEASURE_DEV_ID_OPA_2, opcode);
            break;
#endif /* defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032) */

        case AIP_OPCODE_TEST_ADC_OPEN:
            break;
        case AIP_OPCODE_TEST_ADC_CLOSE:
            rval = adc_stop(MEASURE_DEV_ID_ADC_1, opcode);
            if( rval )      break;

            rval = adc_deinit();
            break;
        case AIP_OPCODE_TEST_AIN_00:
        case AIP_OPCODE_TEST_AIN_01:
        case AIP_OPCODE_TEST_AIN_02:
        case AIP_OPCODE_TEST_AIN_03:
        case AIP_OPCODE_TEST_AIN_04:
        case AIP_OPCODE_TEST_AIN_05:
        case AIP_OPCODE_TEST_AIN_06:
        case AIP_OPCODE_TEST_AIN_07:
#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_AIN_08:
        case AIP_OPCODE_TEST_AIN_09:
        case AIP_OPCODE_TEST_AIN_10:
        case AIP_OPCODE_TEST_AIN_11:
        case AIP_OPCODE_TEST_AIN_12:
        case AIP_OPCODE_TEST_AIN_13:
        case AIP_OPCODE_TEST_AIN_14:
        case AIP_OPCODE_TEST_AIN_16:
        case AIP_OPCODE_TEST_AIN_17:
        case AIP_OPCODE_TEST_AIN_18:
        case AIP_OPCODE_TEST_AIN_19:
        case AIP_OPCODE_TEST_AIN_20:
        case AIP_OPCODE_TEST_AIN_21:
        case AIP_OPCODE_TEST_AIN_22:
    #if defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_AIN_15:
        case AIP_OPCODE_TEST_AIN_23:
    #endif
#endif  /* defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032) */
            rval = adc_init(MEASURE_DEV_ID_ADC_1, opcode, pPackage);
            if( rval )      break;

            rval = adc_start(MEASURE_DEV_ID_ADC_1, opcode, pPackage);
            break;

        case AIP_OPCODE_TEST_VC0_IN_00:
        case AIP_OPCODE_TEST_VC0_IN_01:
        case AIP_OPCODE_TEST_VC0_IN_02:
#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_VC0_IN_03:
        case AIP_OPCODE_TEST_VC0_IN_04:
        case AIP_OPCODE_TEST_VC0_IN_05:
        case AIP_OPCODE_TEST_VC0_IN_06:
        case AIP_OPCODE_TEST_VC0_IN_07:
        case AIP_OPCODE_TEST_VC0_IN_08:
        case AIP_OPCODE_TEST_VC0_IN_09:
        case AIP_OPCODE_TEST_VC0_IN_10:
        case AIP_OPCODE_TEST_VC0_IN_11:
        case AIP_OPCODE_TEST_VC0_IN_12:
        case AIP_OPCODE_TEST_VC0_IN_13:
        case AIP_OPCODE_TEST_VC0_IN_14:
        case AIP_OPCODE_TEST_VC0_IN_15:
        case AIP_OPCODE_TEST_VC1_IN_00:
        case AIP_OPCODE_TEST_VC1_IN_01:
        case AIP_OPCODE_TEST_VC1_IN_02:
        case AIP_OPCODE_TEST_VC1_IN_03:
        case AIP_OPCODE_TEST_VC1_IN_04:
        case AIP_OPCODE_TEST_VC1_IN_05:
        case AIP_OPCODE_TEST_VC1_IN_06:
        case AIP_OPCODE_TEST_VC1_IN_07:
        case AIP_OPCODE_TEST_VC1_IN_08:
        case AIP_OPCODE_TEST_VC1_IN_09:
        case AIP_OPCODE_TEST_VC1_IN_10:
        case AIP_OPCODE_TEST_VC1_IN_11:
        case AIP_OPCODE_TEST_VC1_IN_12:
        case AIP_OPCODE_TEST_VC1_IN_13:
        case AIP_OPCODE_TEST_VC1_IN_14:
        case AIP_OPCODE_TEST_VC1_IN_15:
#endif  /* defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032) */
            {

            }
            break;
    }
    return rval;
}

measure_err_t
measure_get_report(ad_package_t *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;
    aip_opcode_t        opcode = (aip_opcode_t)pPackage->base.opcode;

    memset(pPackage, 0x0, sizeof(ad_package_t));

    pPackage->base.opcode = opcode;

    switch( opcode )
    {
        default:
            rval = MEASURE_ERR_UNKNOWN_OPCODE;
            pPackage->base.err_code = AIP_ERR_NO_OPCODE;
            break;

        case AIP_OPCODE_TEST_AIN_00:
        case AIP_OPCODE_TEST_AIN_01:
        case AIP_OPCODE_TEST_AIN_02:
        case AIP_OPCODE_TEST_AIN_03:
        case AIP_OPCODE_TEST_AIN_04:
        case AIP_OPCODE_TEST_AIN_05:
        case AIP_OPCODE_TEST_AIN_06:
        case AIP_OPCODE_TEST_AIN_07:
#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_AIN_08:
        case AIP_OPCODE_TEST_AIN_09:
        case AIP_OPCODE_TEST_AIN_10:
        case AIP_OPCODE_TEST_AIN_11:
        case AIP_OPCODE_TEST_AIN_12:
        case AIP_OPCODE_TEST_AIN_13:
        case AIP_OPCODE_TEST_AIN_14:
        case AIP_OPCODE_TEST_AIN_16:
        case AIP_OPCODE_TEST_AIN_17:
        case AIP_OPCODE_TEST_AIN_18:
        case AIP_OPCODE_TEST_AIN_19:
        case AIP_OPCODE_TEST_AIN_20:
        case AIP_OPCODE_TEST_AIN_21:
        case AIP_OPCODE_TEST_AIN_22:
    #if defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_AIN_15:
        case AIP_OPCODE_TEST_AIN_23:
    #endif
#endif  /* defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032) */
            {
                uint32_t    conv_value = 0;
                rval = adc_get_value(MEASURE_DEV_ID_ADC_1, opcode, &conv_value);
                if( rval )  break;

                pPackage->u.adc.conv_value = (uint16_t)conv_value & 0xFFFF;
            }
            break;

        case AIP_OPCODE_TEST_VC0_IN_00:
        case AIP_OPCODE_TEST_VC0_IN_01:
        case AIP_OPCODE_TEST_VC0_IN_02:
#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_VC0_IN_03:
        case AIP_OPCODE_TEST_VC0_IN_04:
        case AIP_OPCODE_TEST_VC0_IN_05:
        case AIP_OPCODE_TEST_VC0_IN_06:
        case AIP_OPCODE_TEST_VC0_IN_07:
        case AIP_OPCODE_TEST_VC0_IN_08:
        case AIP_OPCODE_TEST_VC0_IN_09:
        case AIP_OPCODE_TEST_VC0_IN_10:
        case AIP_OPCODE_TEST_VC0_IN_11:
        case AIP_OPCODE_TEST_VC0_IN_12:
        case AIP_OPCODE_TEST_VC0_IN_13:
        case AIP_OPCODE_TEST_VC0_IN_14:
        case AIP_OPCODE_TEST_VC0_IN_15:
        case AIP_OPCODE_TEST_VC1_IN_00:
        case AIP_OPCODE_TEST_VC1_IN_01:
        case AIP_OPCODE_TEST_VC1_IN_02:
        case AIP_OPCODE_TEST_VC1_IN_03:
        case AIP_OPCODE_TEST_VC1_IN_04:
        case AIP_OPCODE_TEST_VC1_IN_05:
        case AIP_OPCODE_TEST_VC1_IN_06:
        case AIP_OPCODE_TEST_VC1_IN_07:
        case AIP_OPCODE_TEST_VC1_IN_08:
        case AIP_OPCODE_TEST_VC1_IN_09:
        case AIP_OPCODE_TEST_VC1_IN_10:
        case AIP_OPCODE_TEST_VC1_IN_11:
        case AIP_OPCODE_TEST_VC1_IN_12:
        case AIP_OPCODE_TEST_VC1_IN_13:
        case AIP_OPCODE_TEST_VC1_IN_14:
        case AIP_OPCODE_TEST_VC1_IN_15:
#endif  /* defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032) */
            {
                int         idx = opcode - AIP_OPCODE_TEST_AIN_00;

                pPackage->base.err_code    = g_report[idx].err_code;
                pPackage->u.vc.ref_voltage = g_report[idx].value;
            }
            break;
    }

    return rval;
}
