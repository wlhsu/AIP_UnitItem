/**
 ******************************************************************************
 * @file    main.c
 * @author  MCU Team
 * @Version V1.0.0
 * @Date    2022/01/19
 * @brief   main function
 ******************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include "main.h"
#include "probe.h"
#include "measure.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
#define CONFIG_DATA_BUF_SIZE            128
//=============================================================================
//                  Macro Definition
//=============================================================================
#define CONFIG_DUT_ID                   FOURCC('d', 'u', 't', CONFIG_I2C_DEV_ADDR)
//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
#if defined(CONFIG_SIM)
#include <windows.h>

#else
I2C_HandleTypeDef   g_hI2C = {0};
#endif // defined

static measure_err_t    g_prev_err_code = MEASURE_ERR_OK;
static uint32_t         g_data_buf[CONFIG_DATA_BUF_SIZE >> 2] = {0};

//=============================================================================
//                  Private Function Definition
//=============================================================================
#if defined(CONFIG_SIM)

#include "fake_api.h"

#define probe_init                  probe_slv_init
#define probe_deinit                probe_slv_deinit
#define probe_read                  probe_slv_read
#define probe_write                 probe_slv_write

static probe_phy_t        g_probe_phy = {0};

#else

/**
 * @brief System Clock Configuration
 * @retval None
 */
static void _SystemClock_Config(void)
{
    RCC_OscInitTypeDef  RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef  RCC_ClkInitStruct = {0};

    RCC_OscInitStruct.OscillatorType       = RCC_OSCILLATORTYPE_HIRC;
    RCC_OscInitStruct.HIRCState            = RCC_HIRC_ON;
    RCC_OscInitStruct.HIRCCalibrationValue = RCC_HIRCCALIBRATION_24M;

    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    /* Initializes the CPU, AHB and APB buses clocks */
    RCC_ClkInitStruct.ClockType     = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK;
    RCC_ClkInitStruct.SYSCLKSource  = RCC_SYSCLKSOURCE_HIRC;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APBCLKDivider = RCC_PCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    return;
}


static probe_err_t
_probe_init(void)
{
    probe_err_t     rval = PROBE_ERR_OK;

    g_hI2C.Instance        = I2C0;
    g_hI2C.Init.Mode       = HAL_I2C_MODE_SLAVE;
    g_hI2C.Init.SpeedClock = 100;
    g_hI2C.Init.SlaveAddr  = (CONFIG_I2C_DEV_ADDR >> 1);
    g_hI2C.Init.BroadAck   = I2C_BROAD_ACK_DISABLE;
    g_hI2C.State           = HAL_I2C_STATE_RESET;

    if( HAL_I2C_Init(&g_hI2C) != HAL_OK )
    {
        err("I2C initial fail !\n");
        rval = PROBE_ERR_PHY_INIT_FAIL;
    }

    return rval;
}

static probe_err_t
_probe_deinit(void)
{
    probe_err_t     rval = PROBE_ERR_OK;
    return rval;
}

static probe_err_t
_probe_read(uint8_t *pData, uint32_t *pLength, void *pExtra)
{
    probe_err_t         rval = PROBE_ERR_OK;
    HAL_StatusTypeDef   rst = HAL_OK;
    uint16_t            data_len = (uint16_t)(*pLength);

    rst = HAL_I2C_Slave_Receive(&g_hI2C, pData, &data_len, HAL_I2C_BLOCKING);
    if( rst != HAL_OK )
    {
        err("DUT phy read fail\n");
        rval = PROBE_ERR_PHY_READ_FAIL;
    }

    *pLength = data_len;
    return rval;
}

static probe_err_t
_probe_write(uint8_t *pData, uint32_t length, void *pExtra)
{
    probe_err_t         rval = PROBE_ERR_OK;
    HAL_StatusTypeDef   rst = HAL_OK;

    rst = HAL_I2C_Slave_Transmit(&g_hI2C, pData, length, HAL_I2C_BLOCKING);
    if( rst != HAL_OK )
    {
        err("DUT phy write fail\n");
        rval = PROBE_ERR_PHY_WRITE_FAIL;
    }
    return rval;
}

static probe_phy_t        g_probe_phy =
{
    .cb_init   = _probe_init,
    .cb_deinit = _probe_deinit,
    .cb_read   = _probe_read,
    .cb_write  = _probe_write,
};
#endif // defined
//=============================================================================
//                  Public Function Definition
//=============================================================================
/**
 * @brief  The application entry point.
 * @retval int
 */

int dut_proc(void)
{
    probe_err_t     rst = PROBE_ERR_OK;

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock to HIRC 24MHz*/
    _SystemClock_Config();

    /* Configure uart1 for printf */
    LogInit();
    msg("\nL030 AIP Measurer %s\n", __TIME__);

    measure_init();

    rst = probe_init((probe_phy_t*)&g_probe_phy);
    if( rst )
    {
        err("DUT probe phy fail \n");
        Error_Handler();
    }

    g_prev_err_code = MEASURE_ERR_OK;

    while (1)
    {
        measure_err_t   err_code = MEASURE_ERR_OK;
        probe_cmd_t     cmd = PROBE_CMD_NONE;
        uint32_t        dut_id = CONFIG_DUT_ID;
        uint32_t        buf_len = sizeof(g_data_buf);
        ad_package_t    *pADPackage = 0;

        rst = probe_read(&cmd, (uint8_t*)&g_data_buf, &buf_len, 0);
        if( rst != PROBE_ERR_OK )
        {
            err("DUT phy read fail \n");
            continue;
        }

        pADPackage = (ad_package_t*)&g_data_buf;

        switch( cmd )
        {
            default:
                info("DUT get unknown 0x%02X\n", cmd);
                break;
            case PROBE_CMD_PING:
                rst = probe_write(PROBE_CMD_ACK, (uint8_t*)&dut_id, sizeof(dut_id), 0);
                if( rst != PROBE_ERR_OK )
                {
                    err("DUT send ACK fail \n");
                    err_code = MEASURE_ERR_PROBE_WRITE_FAIL;
                }
                break;

            case PROBE_CMD_REQ_EXEC_OPCODE:
                rst = probe_write(PROBE_CMD_ACK, (uint8_t*)&dut_id, sizeof(dut_id), 0);
                if( rst != PROBE_ERR_OK )
                {
                    err("DUT send ACK fail \n");
                    err_code = MEASURE_ERR_PROBE_WRITE_FAIL;
                }

                info("DUT start process (0x%X) ...\n", pADPackage->base.opcode);
                err_code = measure_proc(pADPackage);
                if( err_code )
                {
                    err("DUT measure fail (err: %d)\n", err_code);
                }
                break;

            case PROBE_CMD_GET_STATUS:
                {
                    ad_package_base_t   *pBase = &pADPackage->base;

                    pBase->err_code = g_prev_err_code;
                    rst = probe_write(PROBE_CMD_ACK, (uint8_t*)pBase, sizeof(ad_package_base_t), 0);
                    if( rst != PROBE_ERR_OK )
                    {
                        err("DUT send report fail \n");
                        err_code = MEASURE_ERR_PROBE_WRITE_FAIL;
                    }
                }
                break;

            case PROBE_CMD_REQ_REPORT:
                err_code = measure_get_report(pADPackage);
                if( err_code )
                {
                    err("DUT get report fail \n");
                }

                rst = probe_write(PROBE_CMD_ACK, (uint8_t*)pADPackage, sizeof(ad_package_t), 0);
                if( rst != PROBE_ERR_OK )
                {
                    err("DUT send report fail \n");
                    err_code = MEASURE_ERR_PROBE_WRITE_FAIL;
                }
                break;
        }

        g_prev_err_code = err_code;
    }
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    __BKPT(1);
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(const char *func_name, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /**
     *  User can add his own implementation to report the file name and line number,
     * e.g. printf("Wrong parameters value: func %s on line %d\n", file, line)
     */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */


