/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file opa.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/21
 * @license
 * @description
 */


#include "opa.h"
#include "main.h"
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
#if defined(CONFIG_SIM) /* Simulation */
#include "fake_api.h"

#else
#endif // defined

static HAL_OPA_HandleTypeDef       g_hOPA = {0};
//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
opa_init(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    g_hOPA.Instance             = (OPA_TypeDef*)OPA;
    g_hOPA.Init.Vref            = (pPackage->u.opa.vref == AIP_OPA_DAC_VREF_VCAP)
                                ? OPA_DAC_VREF_VCAP : OPA_DAC_VREF_VDD;
    g_hOPA.Init.DC_Level_Step   = pPackage->u.opa.dc_level;

    if( dev_id == MEASURE_DEV_ID_OPA_1 ||
        dev_id == MEASURE_DEV_ID_OPA_ALL )
    {
        g_hOPA.Init.OP1_Mode = OPA_MODE_NONE;
        g_hOPA.Init.OP1_Out  = OPA_Out_IO_PIN;
    }

    if( dev_id == MEASURE_DEV_ID_OPA_2 ||
        dev_id == MEASURE_DEV_ID_OPA_ALL )
    {
    #if defined(CONFIG_USE_ZB32L030)
        g_hOPA.Init.OP2_Mode = OPA_MODE_STANDALONE;
    #elif defined(CONFIG_USE_ZB32L032)
        g_hOPA.Init.OP2_Mode = OPA_MODE_NONE;
    #endif

        g_hOPA.Init.OP2_Out  = OPA_Out_IO_PIN;
    }

    if( HAL_OPA_Init(&g_hOPA) != HAL_OK )
    {
        err("OPA init fail \n");
        rval = MEASURE_ERR_DEV_INIT_FAIL;
    }

    return rval;
}

measure_err_t
opa_deinit(void)
{
    measure_err_t       rval = MEASURE_ERR_OK;
    HAL_OPA_DeInit(&g_hOPA);
    return rval;
}


measure_err_t
opa_start(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    OPA_IdTypeDef           target_id = OPA_ID_1;
    OPA_ConfigTypeDef       opa_cfg = { .DC_Level_Step = 0,};

    opa_cfg.OP_Id         = (dev_id == MEASURE_DEV_ID_OPA_1) ? OPA_ID_1 :
                            (dev_id == MEASURE_DEV_ID_OPA_2) ? OPA_ID_2 : OPA_ID_ALL;
    opa_cfg.OutType       = OPA_Out_IO_PIN;
    opa_cfg.DC_Level_Step = pPackage->u.opa.dc_level;

    HAL_OPA_Stop(&g_hOPA, OPA_ID_ALL);

    switch( opcode )
    {
        default:
            return MEASURE_ERR_WRONG_PARAM;

        case AIP_OPCODE_TEST_OPA_CASCADING:
            // ToDo: configurate OPA1/OPA2
            break;

        /* OPA1 */
        case AIP_OPCODE_TEST_OPA1_CALIBRATION:  opa_cfg.Mode = OPA_MODE_CALIBRATION;        break;
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_INP: opa_cfg.Mode = OPA_MODE_FOLLOWER_INP;       break;
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC: opa_cfg.Mode = OPA_MODE_FOLLOWER_DAC;       break;
        case AIP_OPCODE_TEST_OPA1_INVERTING:    opa_cfg.Mode = OPA_MODE_PGA_INVERTING;      break;
        case AIP_OPCODE_TEST_OPA1_NONINVERTING: opa_cfg.Mode = OPA_MODE_PGA_NONINVERTING;   break;
        case AIP_OPCODE_TEST_OPA1_DIFF:         opa_cfg.Mode = OPA_MODE_PGA_DIFF;           break;

        /* OPA2 */
    #if defined(CONFIG_USE_ZB32L030)
        case AIP_OPCODE_TEST_OPA2_CALIBRATION:
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA2_DIFF:
        case AIP_OPCODE_TEST_OPA2_INVERTING:
        case AIP_OPCODE_TEST_OPA2_NONINVERTING:
            target_id    = OPA_ID_2;
            opa_cfg.Mode = OPA_MODE_STANDALONE;
            break;
    #elif defined(CONFIG_USE_ZB32L032)
        case AIP_OPCODE_TEST_OPA2_CALIBRATION:  opa_cfg.Mode = OPA_MODE_CALIBRATION;        break;
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP: opa_cfg.Mode = OPA_MODE_FOLLOWER_INP;       break;
        case AIP_OPCODE_TEST_OPA2_DIFF:         opa_cfg.Mode = OPA_MODE_PGA_DIFF;           break;
        case AIP_OPCODE_TEST_OPA2_INVERTING:    opa_cfg.Mode = OPA_MODE_PGA_INVERTING;      break;
        case AIP_OPCODE_TEST_OPA2_NONINVERTING: opa_cfg.Mode = OPA_MODE_PGA_NONINVERTING;   break;
    #endif
    }

    HAL_OPA_Config(&g_hOPA, &opa_cfg);
    HAL_OPA_Start(&g_hOPA, target_id);

    return rval;
}

measure_err_t
opa_stop(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    OPA_IdTypeDef   opa_id = OPA_ID_1;

    if( dev_id == MEASURE_DEV_ID_OPA_1 )
        opa_id = OPA_ID_1;
    else if( dev_id == MEASURE_DEV_ID_OPA_2 )
        opa_id = OPA_ID_2;
    else
        return MEASURE_ERR_WRONG_PARAM;

    HAL_OPA_Stop(&g_hOPA, opa_id);

    return rval;
}

