/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file adc.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/13
 * @license
 * @description
 */


#include "adc.h"
#include "main.h"

#if defined(CONFIG_SIM) /* Simulation */
#include "fake_api.h"

#else
#endif // defined
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
static ADC_HandleTypeDef       g_hADC = {0};
//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
adc_init(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    g_hADC.Init.AChannelSel = (opcode == AIP_OPCODE_TEST_AIN_00) ? HAL_ADC_CHANNEL_0  :
                              (opcode == AIP_OPCODE_TEST_AIN_01) ? HAL_ADC_CHANNEL_1  :
                              (opcode == AIP_OPCODE_TEST_AIN_02) ? HAL_ADC_CHANNEL_2  :
                              (opcode == AIP_OPCODE_TEST_AIN_03) ? HAL_ADC_CHANNEL_3  :
                              (opcode == AIP_OPCODE_TEST_AIN_04) ? HAL_ADC_CHANNEL_4  :
                              (opcode == AIP_OPCODE_TEST_AIN_05) ? HAL_ADC_CHANNEL_5  :
                              (opcode == AIP_OPCODE_TEST_AIN_06) ? HAL_ADC_CHANNEL_6  :
                              (opcode == AIP_OPCODE_TEST_AIN_07) ? HAL_ADC_CHANNEL_7  :
                              (opcode == AIP_OPCODE_TEST_AIN_08) ? HAL_ADC_CHANNEL_8  :
                              (opcode == AIP_OPCODE_TEST_AIN_09) ? HAL_ADC_CHANNEL_9  :
                              (opcode == AIP_OPCODE_TEST_AIN_10) ? HAL_ADC_CHANNEL_10 :
                              (opcode == AIP_OPCODE_TEST_AIN_11) ? HAL_ADC_CHANNEL_11 :
                              (opcode == AIP_OPCODE_TEST_AIN_12) ? HAL_ADC_CHANNEL_12 :
                              (opcode == AIP_OPCODE_TEST_AIN_13) ? HAL_ADC_CHANNEL_13 :
                              (opcode == AIP_OPCODE_TEST_AIN_14) ? HAL_ADC_CHANNEL_14 :
                              (opcode == AIP_OPCODE_TEST_AIN_16) ? HAL_ADC_CHANNEL_16 :
                              (opcode == AIP_OPCODE_TEST_AIN_17) ? HAL_ADC_CHANNEL_17 :
                              (opcode == AIP_OPCODE_TEST_AIN_18) ? HAL_ADC_CHANNEL_18 :
                              (opcode == AIP_OPCODE_TEST_AIN_19) ? HAL_ADC_CHANNEL_19 :
                              (opcode == AIP_OPCODE_TEST_AIN_20) ? HAL_ADC_CHANNEL_20 :
                              (opcode == AIP_OPCODE_TEST_AIN_21) ? HAL_ADC_CHANNEL_21 :
                            #if defined(CONFIG_USE_ZB32L032)
                              (opcode == AIP_OPCODE_TEST_AIN_15) ? HAL_ADC_CHANNEL_15 :
                              (opcode == AIP_OPCODE_TEST_AIN_23) ? HAL_ADC_CHANNEL_23 :
                            #endif
                              HAL_ADC_CHANNEL_22;


    g_hADC.Init.SamplingTime    = (pPackage->u.adc.cfg.duty_cycle == AIP_ADC_DUTY_CYCLE_4)
                                ? HAL_ADC_SAMPLE_4CYCLE : HAL_ADC_SAMPLE_8CYCLE;

    g_hADC.Init.ClkSel  = (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_1  ) ? HAL_ADC_CLOCK_PCLK_DIV1 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_2  ) ? HAL_ADC_CLOCK_PCLK_DIV2 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_4  ) ? HAL_ADC_CLOCK_PCLK_DIV4 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_8  ) ? HAL_ADC_CLOCK_PCLK_DIV8 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_16 ) ? HAL_ADC_CLOCK_PCLK_DIV16 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_32 ) ? HAL_ADC_CLOCK_PCLK_DIV32 :
                          (pPackage->u.adc.cfg.clk_div == AIP_ADC_CLK_DIV_64 ) ? HAL_ADC_CLOCK_PCLK_DIV64 :
                          HAL_ADC_CLOCK_PCLK_DIV128;

    g_hADC.Init.Vref    = (pPackage->u.adc.cfg.vref == AIP_ADC_VREF_VCAP) ? HAL_ADC_VREF_VCAP :
                          (pPackage->u.adc.cfg.vref == AIP_ADC_VREF_VDD ) ? HAL_ADC_VREF_VDD :
                          HAL_ADC_VREF_EXT_VREF;

    g_hADC.Init.ClkShift    = (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_0NS_PE  ) ?  HAL_ADC_SAMPLE_SHIFT_0NS_PE  :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_4NS_PE  ) ?  HAL_ADC_SAMPLE_SHIFT_4NS_PE  :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_8NS_PE  ) ?  HAL_ADC_SAMPLE_SHIFT_8NS_PE  :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_12NS_PE ) ?  HAL_ADC_SAMPLE_SHIFT_12NS_PE :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_0NS_NE  ) ?  HAL_ADC_SAMPLE_SHIFT_0NS_NE  :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_4NS_NE  ) ?  HAL_ADC_SAMPLE_SHIFT_4NS_NE  :
                              (pPackage->u.adc.cfg.clk_shift == AIP_ADC_CLK_SHFT_8NS_NE  ) ?  HAL_ADC_SAMPLE_SHIFT_8NS_NE  :
                              HAL_ADC_SAMPLE_SHIFT_16NS_NE;

    g_hADC.Instance                = (ADC_TypeDef*)ADC;
    g_hADC.Init.ConvMode           = HAL_ADC_MODE_SINGLE;
    g_hADC.Init.AutoAccumulation   = HAL_ADC_AUTOACC_DISABLE;
    g_hADC.Init.CircleMode         = HAL_ADC_MULTICHANNEL_NONCIRCLE;
    g_hADC.Init.ExtTrigConv0       = HAL_ADC_EXTTRIG_SW_START;

    if( HAL_ADC_Init(&g_hADC) != HAL_OK )
    {
        err("ADC init fail \n");
        rval = MEASURE_ERR_DEV_INIT_FAIL;
    }

    return rval;
}

measure_err_t
adc_deinit(void)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    HAL_ADC_DeInit(&g_hADC);

    return rval;
}


measure_err_t
adc_start(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    if( HAL_ADC_Start(&g_hADC) != HAL_OK )
    {
        err("ADC start fail \n");
        rval = MEASURE_ERR_DEV_PROC_FAIL;
    }

    return rval;
}

measure_err_t
adc_stop(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode)
{
    measure_err_t       rval = MEASURE_ERR_OK;

    if( HAL_ADC_Stop(&g_hADC) != HAL_OK )
    {
        err("ADC stop fail \n");
        rval = MEASURE_ERR_DEV_DEINIT_FAIL;
    }

    return rval;
}

measure_err_t
adc_get_value(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    uint32_t            *pValue)
{
    measure_err_t               rval = MEASURE_ERR_OK;
    HAL_ADC_ChannelSelTypeDef   channel = HAL_ADC_CHANNEL_0;

    if( !pValue )
        return MEASURE_ERR_NULL_POINTER;

    channel = (opcode == AIP_OPCODE_TEST_AIN_00) ? HAL_ADC_CHANNEL_0  :
              (opcode == AIP_OPCODE_TEST_AIN_01) ? HAL_ADC_CHANNEL_1  :
              (opcode == AIP_OPCODE_TEST_AIN_02) ? HAL_ADC_CHANNEL_2  :
              (opcode == AIP_OPCODE_TEST_AIN_03) ? HAL_ADC_CHANNEL_3  :
              (opcode == AIP_OPCODE_TEST_AIN_04) ? HAL_ADC_CHANNEL_4  :
              (opcode == AIP_OPCODE_TEST_AIN_05) ? HAL_ADC_CHANNEL_5  :
              (opcode == AIP_OPCODE_TEST_AIN_06) ? HAL_ADC_CHANNEL_6  :
              (opcode == AIP_OPCODE_TEST_AIN_07) ? HAL_ADC_CHANNEL_7  :
              (opcode == AIP_OPCODE_TEST_AIN_08) ? HAL_ADC_CHANNEL_8  :
              (opcode == AIP_OPCODE_TEST_AIN_09) ? HAL_ADC_CHANNEL_9  :
              (opcode == AIP_OPCODE_TEST_AIN_10) ? HAL_ADC_CHANNEL_10 :
              (opcode == AIP_OPCODE_TEST_AIN_11) ? HAL_ADC_CHANNEL_11 :
              (opcode == AIP_OPCODE_TEST_AIN_12) ? HAL_ADC_CHANNEL_12 :
              (opcode == AIP_OPCODE_TEST_AIN_13) ? HAL_ADC_CHANNEL_13 :
              (opcode == AIP_OPCODE_TEST_AIN_14) ? HAL_ADC_CHANNEL_14 :
              (opcode == AIP_OPCODE_TEST_AIN_16) ? HAL_ADC_CHANNEL_16 :
              (opcode == AIP_OPCODE_TEST_AIN_17) ? HAL_ADC_CHANNEL_17 :
              (opcode == AIP_OPCODE_TEST_AIN_18) ? HAL_ADC_CHANNEL_18 :
              (opcode == AIP_OPCODE_TEST_AIN_19) ? HAL_ADC_CHANNEL_19 :
              (opcode == AIP_OPCODE_TEST_AIN_20) ? HAL_ADC_CHANNEL_20 :
              (opcode == AIP_OPCODE_TEST_AIN_21) ? HAL_ADC_CHANNEL_21 :
            #if defined(CONFIG_USE_ZB32L032)
              (opcode == AIP_OPCODE_TEST_AIN_15) ? HAL_ADC_CHANNEL_15 :
              (opcode == AIP_OPCODE_TEST_AIN_23) ? HAL_ADC_CHANNEL_23 :
            #endif
              HAL_ADC_CHANNEL_22;

    while( __HAL_ADC_IS_BUSY(&g_hADC) ) {}

    *pValue = HAL_ADC_GetValue(&g_hADC, channel);

    return rval;
}
