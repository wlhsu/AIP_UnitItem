/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file measure.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/12
 * @license
 * @description
 */

#ifndef __measure_H_wBEEf01x_lMZJ_HP6d_sdnK_ummWvLHr9lpC__
#define __measure_H_wBEEf01x_lMZJ_HP6d_sdnK_ummWvLHr9lpC__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "aip_package.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
typedef enum measure_err
{
    MEASURE_ERR_OK                  = ERR_CODE_OK,
    MEASURE_ERR_WRONG_PARAM         = ERR_CODE_WRONG_PARAM,
    MEASURE_ERR_NULL_POINTER        = ERR_CODE_NULL_POINTER,
    MEASURE_ERR_NO_MODULE           = ERR_CODE_NO_MODULE,
    MEASURE_ERR_UNKNOWN_OPCODE      = ERR_CODE_UNKNOWN_OPCODE,
    MEASURE_ERR_DEV_INIT_FAIL       = ERR_CODE_DEV_INIT_FAIL,
    MEASURE_ERR_DEV_PROC_FAIL       = ERR_CODE_DEV_PROC_FAIL,
    MEASURE_ERR_DEV_DEINIT_FAIL     = ERR_CODE_DEV_DEINIT_FAIL,
    MEASURE_ERR_PROBE_READ_FAIL     = ERR_CODE_PROBE_READ_FAIL,
    MEASURE_ERR_PROBE_WRITE_FAIL    = ERR_CODE_PROBE_WRITE_FAIL,
    MEASURE_ERR_UNKNOWN_FAIL        = ERR_CODE_UNKNOWN_FAIL,

} measure_err_t;

typedef enum measure_dev_id
{
    MEASURE_DEV_ID_NONE     = 0,
    MEASURE_DEV_ID_OPA_1,
    MEASURE_DEV_ID_OPA_2,
    MEASURE_DEV_ID_OPA_ALL,
    MEASURE_DEV_ID_ADC_1,
    MEASURE_DEV_ID_ADC_2,
    MEASURE_DEV_ID_VC_0,
    MEASURE_DEV_ID_VC_1,

} measure_dev_id_t;
//=============================================================================
//                  Macro Definition
//=============================================================================
#define stringize(s)    #s
#define _toStr(a)       stringize(a)
//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct measure_opcode_msg
{
    aip_opcode_t    opcode;
    char            *txt;
} measure_opcode_msg_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================
#if defined(CONFIG_SIM) /* Simulation */
static void
_measure_log_opcode(aip_opcode_t opcode)
{
    measure_opcode_msg_t    aip_opcode_list[] = {
        { .opcode = AIP_OPCODE_NONE,                .txt = _toStr(AIP_OPCODE_NONE), },
        { .opcode = AIP_OPCODE_TEST_DAC8562,        .txt = _toStr(AIP_OPCODE_TEST_DAC8562), },
        { .opcode = AIP_OPCODE_TEST_OPA1_OPEN,      .txt = _toStr(AIP_OPCODE_TEST_OPA1_OPEN), },
        { .opcode = AIP_OPCODE_TEST_OPA2_OPEN,      .txt = _toStr(AIP_OPCODE_TEST_OPA2_OPEN), },
        { .opcode = AIP_OPCODE_TEST_AIN_ALL,        .txt = _toStr(AIP_OPCODE_TEST_AIN_ALL), },
        { .opcode = AIP_OPCODE_TEST_VC0_ALL,        .txt = _toStr(AIP_OPCODE_TEST_VC0_ALL), },
        { .opcode = AIP_OPCODE_TEST_VC1_ALL,        .txt = _toStr(AIP_OPCODE_TEST_VC1_ALL), },
        { .opcode = AIP_OPCODE_TEST_OPA_ALL,        .txt = _toStr(AIP_OPCODE_TEST_OPA_ALL), },
        { .opcode = AIP_OPCODE_TEST_AIN_00,         .txt = _toStr(AIP_OPCODE_TEST_AIN_00), },
        { .opcode = AIP_OPCODE_TEST_AIN_01,         .txt = _toStr(AIP_OPCODE_TEST_AIN_01), },
        { .opcode = AIP_OPCODE_TEST_AIN_02,         .txt = _toStr(AIP_OPCODE_TEST_AIN_02), },
        { .opcode = AIP_OPCODE_TEST_AIN_03,         .txt = _toStr(AIP_OPCODE_TEST_AIN_03), },
        { .opcode = AIP_OPCODE_TEST_AIN_04,         .txt = _toStr(AIP_OPCODE_TEST_AIN_04), },
        { .opcode = AIP_OPCODE_TEST_AIN_05,         .txt = _toStr(AIP_OPCODE_TEST_AIN_05), },
        { .opcode = AIP_OPCODE_TEST_AIN_06,         .txt = _toStr(AIP_OPCODE_TEST_AIN_06), },
        { .opcode = AIP_OPCODE_TEST_AIN_07,         .txt = _toStr(AIP_OPCODE_TEST_AIN_07), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_00,      .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_00), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_01,      .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_01), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_02,      .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_02), },
    #if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
        { .opcode = AIP_OPCODE_TEST_AIN_08,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_08), },
        { .opcode = AIP_OPCODE_TEST_AIN_09,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_09), },
        { .opcode = AIP_OPCODE_TEST_AIN_10,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_10), },
        { .opcode = AIP_OPCODE_TEST_AIN_11,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_11), },
        { .opcode = AIP_OPCODE_TEST_AIN_12,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_12), },
        { .opcode = AIP_OPCODE_TEST_AIN_13,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_13), },
        { .opcode = AIP_OPCODE_TEST_AIN_14,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_14), },
//        { .opcode = AIP_OPCODE_TEST_AIN_15,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_15), },
        { .opcode = AIP_OPCODE_TEST_AIN_16,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_16), },
        { .opcode = AIP_OPCODE_TEST_AIN_17,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_17), },
        { .opcode = AIP_OPCODE_TEST_AIN_18,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_18), },
        { .opcode = AIP_OPCODE_TEST_AIN_19,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_19), },
        { .opcode = AIP_OPCODE_TEST_AIN_20,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_20), },
        { .opcode = AIP_OPCODE_TEST_AIN_21,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_21), },
        { .opcode = AIP_OPCODE_TEST_AIN_22,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_22), },
//        { .opcode = AIP_OPCODE_TEST_AIN_23,                     .txt = _toStr(AIP_OPCODE_TEST_AIN_23), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_03,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_03), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_04,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_04), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_05,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_05), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_06,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_06), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_07,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_07), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_08,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_08), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_09,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_09), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_10,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_10), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_11,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_11), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_12,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_12), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_13,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_13), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_14,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_14), },
        { .opcode = AIP_OPCODE_TEST_VC0_IN_15,                  .txt = _toStr(AIP_OPCODE_TEST_VC0_IN_15), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_00,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_00), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_01,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_01), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_02,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_02), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_03,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_03), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_04,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_04), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_05,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_05), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_06,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_06), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_07,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_07), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_08,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_08), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_09,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_09), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_10,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_10), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_11,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_11), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_12,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_12), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_13,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_13), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_14,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_14), },
        { .opcode = AIP_OPCODE_TEST_VC1_IN_15,                  .txt = _toStr(AIP_OPCODE_TEST_VC1_IN_15), },
        { .opcode = AIP_OPCODE_TEST_OPA1_CALIBRATION,           .txt = _toStr(AIP_OPCODE_TEST_OPA1_CALIBRATION), },
        { .opcode = AIP_OPCODE_TEST_OPA1_FOLLOWER_INP,          .txt = _toStr(AIP_OPCODE_TEST_OPA1_FOLLOWER_INP), },
        { .opcode = AIP_OPCODE_TEST_OPA1_INVERTING,             .txt = _toStr(AIP_OPCODE_TEST_OPA1_INVERTING), },
        { .opcode = AIP_OPCODE_TEST_OPA1_NONINVERTING,          .txt = _toStr(AIP_OPCODE_TEST_OPA1_NONINVERTING), },
        { .opcode = AIP_OPCODE_TEST_OPA1_DIFF,                  .txt = _toStr(AIP_OPCODE_TEST_OPA1_DIFF), },
        { .opcode = AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC,          .txt = _toStr(AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC), },
        { .opcode = AIP_OPCODE_TEST_OPA2_CALIBRATION,           .txt = _toStr(AIP_OPCODE_TEST_OPA2_CALIBRATION), },
        { .opcode = AIP_OPCODE_TEST_OPA2_FOLLOWER_INP,          .txt = _toStr(AIP_OPCODE_TEST_OPA2_FOLLOWER_INP), },
        { .opcode = AIP_OPCODE_TEST_OPA2_INVERTING,             .txt = _toStr(AIP_OPCODE_TEST_OPA2_INVERTING), },
        { .opcode = AIP_OPCODE_TEST_OPA2_NONINVERTING,          .txt = _toStr(AIP_OPCODE_TEST_OPA2_NONINVERTING), },
        { .opcode = AIP_OPCODE_TEST_OPA2_DIFF,                  .txt = _toStr(AIP_OPCODE_TEST_OPA2_DIFF), },
        { .opcode = AIP_OPCODE_TEST_OPA_CASCADING,              .txt = _toStr(AIP_OPCODE_TEST_OPA_CASCADING), },

    #endif
    };

    for(int i = 0; i < sizeof(aip_opcode_list)/sizeof(aip_opcode_list[0]); i++)
    {
        if( opcode == aip_opcode_list[i].opcode )
        {
            printf("%s\n", aip_opcode_list[i].txt);
            break;
        }
    }
    return;
}
#else
#define _measure_log_opcode(opcode)

#endif  /* defined(CONFIG_SIM) */
//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
measure_init(void);


measure_err_t
measure_proc(ad_package_t *pPackage);


measure_err_t
measure_get_report(ad_package_t *pPackage);



#ifdef __cplusplus
}
#endif

#endif
