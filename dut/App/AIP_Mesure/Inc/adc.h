/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file adc.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/13
 * @license
 * @description
 */

#ifndef __adc_H_wuCTT5iu_lIPR_Hies_swm0_ui0WtM7UeL7V__
#define __adc_H_wuCTT5iu_lIPR_Hies_swm0_ui0WtM7UeL7V__

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "measure.h"
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
adc_init(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage);

measure_err_t
adc_deinit(void);


measure_err_t
adc_start(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage);


measure_err_t
adc_stop(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode);

measure_err_t
adc_get_value(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    uint32_t            *pValue);


#ifdef __cplusplus
}
#endif

#endif
