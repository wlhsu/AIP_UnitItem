/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file opa.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/21
 * @license
 * @description
 */

#ifndef __opa_H_wRM9iF52_lLnU_HSl0_s78I_uG52mvC8Ks15__
#define __opa_H_wRM9iF52_lLnU_HSl0_s78I_uG52mvC8Ks15__

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "measure.h"
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
measure_err_t
opa_init(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage);


measure_err_t
opa_deinit(void);

measure_err_t
opa_start(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode,
    ad_package_t        *pPackage);


measure_err_t
opa_stop(
    measure_dev_id_t    dev_id,
    aip_opcode_t        opcode);


#ifdef __cplusplus
}
#endif

#endif
