/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file err_code.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/23
 * @license
 * @description
 */

#ifndef __err_code_H_wXOPMx4H_lVV5_HgjZ_s4rm_u6tyM4eeyyRl__
#define __err_code_H_wXOPMx4H_lVV5_HgjZ_s4rm_u6tyM4eeyyRl__

#ifdef __cplusplus
extern "C" {
#endif


//=============================================================================
//                  Constant Definition
//=============================================================================
typedef enum err_code
{
    /* status */
    ERR_CODE_OK                 = 0,
    ERR_CODE_GET_UITEM,
    ERR_CODE_NO_INCOMING,
    ERR_CODE_BUFFER_FULL,

    /* error */
    ERR_CODE_WRONG_PARAM        = 0xE000,
    ERR_CODE_NULL_POINTER,
    ERR_CODE_NO_MODULE,
    ERR_CODE_UNKNOWN_OPCODE,
    ERR_CODE_OPCODE_EXEC_FAIL,

    ERR_CODE_DEV_INIT_FAIL,
    ERR_CODE_DEV_PROC_FAIL,
    ERR_CODE_DEV_DEINIT_FAIL,

    ERR_CODE_PROBE_INIT_FAIL,
    ERR_CODE_PROBE_READ_FAIL,
    ERR_CODE_PROBE_WRITE_FAIL,
    ERR_CODE_PROBE_CHECKSUM_FAIL,
    ERR_CODE_PROBE_NO_INSTANCE,


    ERR_CODE_UNKNOWN_FAIL,
    ERR_CODE_SET_DAC8562_FAIL,

} err_code_t;
//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================

#ifdef __cplusplus
}
#endif

#endif
