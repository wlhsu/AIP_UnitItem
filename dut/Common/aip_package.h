/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file aip_package.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/04
 * @license
 * @description
 */

#ifndef __ad_package_H_w0glDktW_lqEv_Htvm_sq70_uRYeBM61n7tz__
#define __ad_package_H_w0glDktW_lqEv_Htvm_sq70_uRYeBM61n7tz__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <err_code.h>
//=============================================================================
//                  Constant Definition
//=============================================================================
typedef enum aip_err
{
    AIP_ERR_OK   = 0,
    AIP_ERR_FAIL,
    AIP_ERR_NO_OPCODE,
    AIP_ERR_FAKE_RESULT = 0x5,

} aip_err_t;

typedef enum aip_opcode
{
    AIP_OPCODE_NONE              = 0,

    AIP_OPCODE_TEST_DAC8562      = 0xC000,
    AIP_OPCODE_TEST_OPA1_OPEN,
    AIP_OPCODE_TEST_OPA2_OPEN,
    AIP_OPCODE_TEST_OPA1_CLOSE,
    AIP_OPCODE_TEST_OPA2_CLOSE,
    AIP_OPCODE_TEST_ADC_OPEN,
    AIP_OPCODE_TEST_ADC_CLOSE,

    //================================
    // module items (group unit items)
    AIP_OPCODE_TEST_AIN_ALL      = 0xA000,
    AIP_OPCODE_TEST_VC0_ALL,
    AIP_OPCODE_TEST_VC1_ALL,
    AIP_OPCODE_TEST_OPA_ALL,

    //================================
    // unit items
    /* L003 */
    AIP_OPCODE_TEST_AIN_00       = 0x5000,
    AIP_OPCODE_TEST_AIN_01,
    AIP_OPCODE_TEST_AIN_02,

    /* L003 */
    AIP_OPCODE_TEST_VC0_IN_00,
    AIP_OPCODE_TEST_VC0_IN_01,
    AIP_OPCODE_TEST_VC0_IN_02,

    /* L03x */
#if defined(CONFIG_USE_ZB32L030) || defined(CONFIG_USE_ZB32L032)
    /* ADC */

    AIP_OPCODE_TEST_AIN_09,
    AIP_OPCODE_TEST_AIN_10,
    AIP_OPCODE_TEST_AIN_11,
    AIP_OPCODE_TEST_AIN_12,
    AIP_OPCODE_TEST_AIN_13,
    AIP_OPCODE_TEST_AIN_14,
    AIP_OPCODE_TEST_AIN_16,
    AIP_OPCODE_TEST_AIN_17,
    AIP_OPCODE_TEST_AIN_20,
    AIP_OPCODE_TEST_AIN_22,

    /* daughter board NO connections */
    AIP_OPCODE_TEST_AIN_03,
    AIP_OPCODE_TEST_AIN_04,
    AIP_OPCODE_TEST_AIN_05,
    AIP_OPCODE_TEST_AIN_06,
    AIP_OPCODE_TEST_AIN_07,
    AIP_OPCODE_TEST_AIN_08,
    AIP_OPCODE_TEST_AIN_18,
    AIP_OPCODE_TEST_AIN_19,
    AIP_OPCODE_TEST_AIN_21,

    #if defined(CONFIG_USE_ZB32L032)
    AIP_OPCODE_TEST_AIN_15,
    AIP_OPCODE_TEST_AIN_23,
    #endif

    /* VC0 */
    AIP_OPCODE_TEST_VC0_IN_03,
    AIP_OPCODE_TEST_VC0_IN_04,
    AIP_OPCODE_TEST_VC0_IN_05,
    AIP_OPCODE_TEST_VC0_IN_06,
    AIP_OPCODE_TEST_VC0_IN_07,
    AIP_OPCODE_TEST_VC0_IN_08,
    AIP_OPCODE_TEST_VC0_IN_09,
    AIP_OPCODE_TEST_VC0_IN_10,
    AIP_OPCODE_TEST_VC0_IN_11,
    AIP_OPCODE_TEST_VC0_IN_12,
    AIP_OPCODE_TEST_VC0_IN_13,
    AIP_OPCODE_TEST_VC0_IN_14,
    AIP_OPCODE_TEST_VC0_IN_15,

    /* VC1 */
    AIP_OPCODE_TEST_VC1_IN_00,
    AIP_OPCODE_TEST_VC1_IN_01,
    AIP_OPCODE_TEST_VC1_IN_02,
    AIP_OPCODE_TEST_VC1_IN_03,
    AIP_OPCODE_TEST_VC1_IN_04,
    AIP_OPCODE_TEST_VC1_IN_05,
    AIP_OPCODE_TEST_VC1_IN_06,
    AIP_OPCODE_TEST_VC1_IN_07,
    AIP_OPCODE_TEST_VC1_IN_08,
    AIP_OPCODE_TEST_VC1_IN_09,
    AIP_OPCODE_TEST_VC1_IN_10,
    AIP_OPCODE_TEST_VC1_IN_11,
    AIP_OPCODE_TEST_VC1_IN_12,
    AIP_OPCODE_TEST_VC1_IN_13,
    AIP_OPCODE_TEST_VC1_IN_14,
    AIP_OPCODE_TEST_VC1_IN_15,

    /* OPA */
    AIP_OPCODE_TEST_OPA1_CALIBRATION,
    AIP_OPCODE_TEST_OPA1_FOLLOWER_INP,
    AIP_OPCODE_TEST_OPA1_INVERTING,
    AIP_OPCODE_TEST_OPA1_NONINVERTING,
    AIP_OPCODE_TEST_OPA1_DIFF,
    AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC,

    AIP_OPCODE_TEST_OPA2_CALIBRATION,
    AIP_OPCODE_TEST_OPA2_FOLLOWER_INP,
    AIP_OPCODE_TEST_OPA2_INVERTING,
    AIP_OPCODE_TEST_OPA2_NONINVERTING,
    AIP_OPCODE_TEST_OPA2_DIFF,

    /* cascading */
    AIP_OPCODE_TEST_OPA_CASCADING,
#endif

    AIP_OPCODE_TEST_END,

} aip_opcode_t;

typedef enum aip_opa_dac_vref
{
    AIP_OPA_DAC_VREF_VCAP = 0,
    AIP_OPA_DAC_VREF_VDD,

} aip_opa_dac_vref_t;

typedef enum aip_opa_mode
{
    AIP_OPA_MODE_NONE   = 0,
    AIP_OPA_MODE_FOLLOWER,
    AIP_OPA_MODE_INVESTING,
    AIP_OPA_MODE_NONINVESTING,
    AIP_OPA_MODE_DIFF,
} aip_opa_mode_t;

typedef enum aip_adc_duty_cycle
{
    AIP_ADC_DUTY_CYCLE_4 = 0,
    AIP_ADC_DUTY_CYCLE_8 = 1,
} aip_adc_duty_cycle_t;

typedef enum aip_adc_vref
{
    AIP_ADC_VREF_VCAP       = 0,
    AIP_ADC_VREF_VDD        = 1,
    AIP_ADC_VREF_EXT_VREF   = 2,
} aip_adc_vref_t;

typedef enum aip_adc_clk_div
{
    AIP_ADC_CLK_DIV_1     = 0,
    AIP_ADC_CLK_DIV_2     = 1,
    AIP_ADC_CLK_DIV_4     = 2,
    AIP_ADC_CLK_DIV_8     = 3,
    AIP_ADC_CLK_DIV_16    = 4,
    AIP_ADC_CLK_DIV_32    = 5,
    AIP_ADC_CLK_DIV_64    = 6,
    AIP_ADC_CLK_DIV_128   = 7

} aip_adc_clk_div_t;

typedef enum aip_adc_clk_shft
{
    AIP_ADC_CLK_SHFT_0NS_PE     = 0,     /*!< No shift with SYSCLK positive edge    */
    AIP_ADC_CLK_SHFT_4NS_PE     = 1,     /*!< Shift 4 ns with SYSCLK positive edge  */
    AIP_ADC_CLK_SHFT_8NS_PE     = 2,     /*!< Shift 8 ns with SYSCLK positive edge  */
    AIP_ADC_CLK_SHFT_12NS_PE    = 3,     /*!< Shift 12 ns with SYSCLK positive edge */
    AIP_ADC_CLK_SHFT_0NS_NE     = 4,     /*!< No shift with SYSCLK negative edge    */
    AIP_ADC_CLK_SHFT_4NS_NE     = 5,     /*!< Shift 4 ns with SYSCLK negative edge  */
    AIP_ADC_CLK_SHFT_8NS_NE     = 6,     /*!< Shift 8 ns with SYSCLK negative edge  */
    AIP_ADC_CLK_SHFT_16NS_NE    = 7,     /*!< Shift 12 ns with SYSCLK negative edge */
} aip_adc_clk_shft_t;
//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================
#if defined ( __CC_ARM )
#pragma anon_unions
#endif

typedef struct ad_package_base
{
    uint16_t    opcode;
    uint16_t    err_code;
} ad_package_base_t;

typedef struct ad_package
{
    ad_package_base_t   base;

    union {
        struct {
            union {
                uint32_t        conv_value;     /*<! ADC conversion value (from DUT) */
                struct {
                    uint32_t    duty_cycle : 2;
                    uint32_t    vref       : 2;
                    uint32_t    clk_div    : 4;
                    uint32_t    clk_shift  : 4;
                } cfg;
            };
        } adc;

        struct {
            float       ref_voltage;
        } vc;

        struct {
            uint8_t     dc_level;
            uint8_t     vref;       /*<!  @ref aip_opa_dac_vref_t */
            uint8_t     opa1_mode;  /*<!  @ref aip_opa_mode_t */
            uint8_t     opa2_mode;  /*<!  @ref aip_opa_mode_t */
        } opa;
    } u;
} ad_package_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================

#ifdef __cplusplus
}
#endif

#endif
