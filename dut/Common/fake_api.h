/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file fake_api.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/23
 * @license
 * @description
 */

#ifndef __fake_api_H_wXOPMx4H_lVV5_HgjZ_s4rm_u6tyM4eeyyRl__
#define __fake_api_H_wXOPMx4H_lVV5_HgjZ_s4rm_u6tyM4eeyyRl__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <windows.h>
//=============================================================================
//                  Constant Definition
//=============================================================================
typedef enum
{
    HAL_OK       = 0x00U,
    HAL_ERROR    = 0x01U,
    HAL_BUSY     = 0x02U,
    HAL_TIMEOUT  = 0x03U
} HAL_StatusTypeDef;


typedef enum multiplexer_chnnl
{
    MULTIPLEXER_CHNNL_Y0 =   0,
    MULTIPLEXER_CHNNL_Y1,
    MULTIPLEXER_CHNNL_Y2,
    MULTIPLEXER_CHNNL_Y3,
    MULTIPLEXER_CHNNL_Y4,
    MULTIPLEXER_CHNNL_Y5,
    MULTIPLEXER_CHNNL_Y6,
    MULTIPLEXER_CHNNL_Y7,
} multiplexer_chnnl_t;

/******* i2c ***************/
#define I2C_HandleTypeDef       int

#define CONFIG_DUT_1_I2C_ADDRESS    0xA0
#define CONFIG_DUT_2_I2C_ADDRESS    0xA2
#define CONFIG_DUT_3_I2C_ADDRESS    0xA4
#define CONFIG_DUT_4_I2C_ADDRESS    0xA8

/******* DAC ***************/
typedef enum DAC8562_CHANNEL
{
    DAC8562_CHANNEL_A   = 0,
    DAC8562_CHANNEL_B,
    DAC8562_CHANNEL_ALL,
} DAC8562_CHANNEL_t;

#define dac8562_set_vout(a, b)          0

/******* ADS ***************/

#define ads1256_get_micro_voltage(a)    11
#define CONIFG_ADS1256_SAMPLE_TIMES         10

#define CONFIG_ADS1256_SPVDD_AIN            0
#define CONFIG_ADS1256_VCAP_AIN             1
#define CONFIG_ADS1256_OPA_AIN              2
#define CONFIG_ADS1256_VDACO1_2             3
#define CONFIG_ADS1256_VDACO2_2             4

typedef enum ads1256_ain
{
    ADS1256_AIN_0   = 0,
    ADS1256_AIN_1,
    ADS1256_AIN_2,
    ADS1256_AIN_3,
    ADS1256_AIN_4,
    ADS1256_AIN_5,
    ADS1256_AIN_6,
    ADS1256_AIN_7,
    ADS1256_AIN_ALL,
} ads1256_ain_t;


/******* OPA ***************/

#define OPA_TypeDef         int
#define OPA                 (int)222
typedef enum OPA_Id
{
    OPA_ID_1 = 0,
    OPA_ID_2,
    OPA_ID_ALL,

} OPA_IdTypeDef;

typedef enum OPA_Mode
{
#define OPA_MODE_PGA_MSK        0xF0u
#define OPA_MODE_PGA_TYPE       0x20u

    OPA_MODE_NONE              = 0x0,

#if defined(CONFIG_USE_ZB32L030)
    OPA_MODE_STANDALONE        = 0x01,                         /*!< L030 OP2 with external gain setting (ONLY OP2 support)         */
#endif  /* CONFIG_USE_ZB32L030 */

    OPA_MODE_CALIBRATION       = 0x02,                         /*!< OP1 calibration mode. User SHOULD measure trim offset manually */
    OPA_MODE_FOLLOWER_INP      = 0x03,                         /*!< OP1 Voltage-Follower (or Unit Gain) mode and OPA1_INP input    */
    OPA_MODE_FOLLOWER_DAC      = 0x04,                         /*!< OP1 Voltage-Follower (or Unit Gain) mode and Vdac input        */
    OPA_MODE_PGA_INVERTING     = (OPA_MODE_PGA_TYPE | 0x00),   /*!< OP1 Inverting Amplifier mode                                   */
    OPA_MODE_PGA_NONINVERTING  = (OPA_MODE_PGA_TYPE | 0x01),   /*!< OP1 Non-Inverting Amplifier mode                               */
    OPA_MODE_PGA_DIFF          = (OPA_MODE_PGA_TYPE | 0x02),   /*!< OP1 Difference Amplifier mode                                  */

} OPA_ModeTypeDef;

typedef enum OPA_DAC_Vref
{
    OPA_DAC_VREF_VCAP = 0x0,
    OPA_DAC_VREF_VDD  = 0x1,

} OPA_DAC_VrefTypeDef;

typedef enum OPA_Out
{
    OPA_Out_ADC    = 0x0,
    OPA_Out_IO_PIN = 0x1,

} OPA_OutTypeDef;

/******* ADC ***************/

#define ADC_TypeDef         int
#define ADC                 (int)333

typedef enum HAL_ADC_SampleDuty
{
    HAL_ADC_SAMPLE_4CYCLE   = 0x00U,
    HAL_ADC_SAMPLE_8CYCLE   = 1,
} HAL_ADC_SampleDutyTypeDef;

typedef enum HAL_ADC_ChannelSel
{
    HAL_ADC_CHANNEL_0        = (0x1 << 0),       /*!< Select ADC channel 0 in single mode            */
    HAL_ADC_CHANNEL_1        = (0x1 << 1),       /*!< Select ADC channel 1 in single mode            */
    HAL_ADC_CHANNEL_2        = (0x1 << 2),       /*!< Select ADC channel 2 in single mode            */
    HAL_ADC_CHANNEL_3        = (0x1 << 3),       /*!< Select ADC channel 3 in single mode            */
    HAL_ADC_CHANNEL_4        = (0x1 << 4),       /*!< Select ADC channel 4 in single mode            */
    HAL_ADC_CHANNEL_5        = (0x1 << 5),       /*!< Select ADC channel 5 in single mode            */
    HAL_ADC_CHANNEL_6        = (0x1 << 6),       /*!< Select ADC channel 6 in single mode            */
    HAL_ADC_CHANNEL_7        = (0x1 << 7),       /*!< Select ADC channel 7 in single mode            */
    HAL_ADC_CHANNEL_8        = (0x1 << 8),       /*!< Select ADC channel 8 in single mode            */
    HAL_ADC_CHANNEL_9        = (0x1 << 9),       /*!< Select ADC channel 9 in single mode            */
    HAL_ADC_CHANNEL_10       = (0x1 << 10),      /*!< Select ADC channel 10 in single mode           */
    HAL_ADC_CHANNEL_11       = (0x1 << 11),      /*!< Select ADC channel 11 in single mode           */
    HAL_ADC_CHANNEL_12       = (0x1 << 12),      /*!< Select ADC channel 12 in single mode           */
    HAL_ADC_CHANNEL_13       = (0x1 << 13),      /*!< Select ADC channel 13 in single mode           */
    HAL_ADC_CHANNEL_14       = (0x1 << 14),      /*!< Select ADC channel 14 in single mode           */
    HAL_ADC_CHANNEL_15       = (0x1 << 15),      /*!< Select ADC channel 15 in single mode           */
    HAL_ADC_CHANNEL_16       = (0x1 << 16),      /*!< Select ADC channel 16 in single mode           */
    HAL_ADC_CHANNEL_17       = (0x1 << 17),      /*!< Select ADC channel 17 in single mode           */
    HAL_ADC_CHANNEL_18       = (0x1 << 18),      /*!< Select ADC channel 18 in single mode           */
    HAL_ADC_CHANNEL_19       = (0x1 << 19),      /*!< Select ADC channel 19 in single mode           */
    HAL_ADC_CHANNEL_20       = (0x1 << 20),      /*!< Select ADC channel 20 in single mode           */
    HAL_ADC_CHANNEL_21       = (0x1 << 21),      /*!< Select ADC channel 21 in single mode           */
    HAL_ADC_CHANNEL_22       = (0x1 << 22),      /*!< Select ADC channel 22 in single mode           */
    HAL_ADC_CHANNEL_23       = (0x1 << 23),      /*!< Select ADC channel 23 in single mode           */

    HAL_ADC_CHANNEL_OP1_O   = HAL_ADC_CHANNEL_10,
    HAL_ADC_CHANNEL_OP2_O   = HAL_ADC_CHANNEL_11,
    HAL_ADC_CHANNEL_DAC_OP  = HAL_ADC_CHANNEL_12,
    HAL_ADC_CHANNEL_V12     = HAL_ADC_CHANNEL_13,
    HAL_ADC_CHANNEL_VCAP    = HAL_ADC_CHANNEL_14,
} HAL_ADC_ChannelSelTypeDef;

typedef enum HAL_ADC_ClkSel
{
    HAL_ADC_CLOCK_PCLK_DIV1     = 0,
    HAL_ADC_CLOCK_PCLK_DIV2     = 1,
    HAL_ADC_CLOCK_PCLK_DIV4     = 2,
    HAL_ADC_CLOCK_PCLK_DIV8     = 3,
    HAL_ADC_CLOCK_PCLK_DIV16    = 4,
    HAL_ADC_CLOCK_PCLK_DIV32    = 5,
    HAL_ADC_CLOCK_PCLK_DIV64    = 6,
    HAL_ADC_CLOCK_PCLK_DIV128   = 7,

} HAL_ADC_ClkSelTypeDef;

typedef enum HAL_ADC_ConvMode
{
    HAL_ADC_MODE_SINGLE     = 0,                /*!< Select ADC single mode          */
    HAL_ADC_MODE_CONTINUE   = 1,                /*!< Select ADC continue mode        */

} HAL_ADC_ConvModeTypeDef;

typedef enum HAL_ADC_CircleMode
{
    HAL_ADC_MULTICHANNEL_NONCIRCLE      = 0,
    HAL_ADC_MULTICHANNEL_CIRCLE         = 1,    /*!< ADC multichannel conversion circle mode   */

} HAL_ADC_CircleModeTypeDef;

typedef enum HAL_ADC_Vref
{
    HAL_ADC_VREF_VDD        = 0,
    HAL_ADC_VREF_EXT_VREF   = 1,
    HAL_ADC_VREF_VCAP       = 2,

} HAL_ADC_VrefTypeDef;

typedef enum HAL_ADC_AutoAcc
{
    HAL_ADC_AUTOACC_DISABLE     = 0,
    HAL_ADC_AUTOACC_ENABLE      = 1,      /*!< Auto accumulation enable       */
} HAL_ADC_AutoAccTypeDef;

typedef enum HAL_ADC_ExtTrigger_Src
{
    HAL_ADC_EXTTRIG_SW_START        = 0x00u,
    HAL_ADC_EXTTRIG_TIM10           = 0x01u,
    HAL_ADC_EXTTRIG_TIM11           = 0x02u,
    HAL_ADC_EXTTRIG_TIM1            = 0x03u,
    HAL_ADC_EXTTRIG_LPTIM           = 0x04u,
    HAL_ADC_EXTTRIG_TIM1_TRGO       = 0x05u,
    HAL_ADC_EXTTRIG_TIM2_TRGO       = 0x06u,
    HAL_ADC_EXTTRIG_TIM2_INT        = 0x07u,
    HAL_ADC_EXTTRIG_UART0_INT       = 0x08u,
    HAL_ADC_EXTTRIG_UART1_INT       = 0x09u,
    HAL_ADC_EXTTRIG_LPUART_INT      = 0x0Au,
    HAL_ADC_EXTTRIG_VC0_INT         = 0x0Bu,
    HAL_ADC_EXTTRIG_VC1_INT         = 0x0Cu,
    HAL_ADC_EXTTRIG_RTC_INT         = 0x0Du,
    HAL_ADC_EXTTRIG_PCA_INT         = 0x0Eu,
    HAL_ADC_EXTTRIG_SPI_INT         = 0x0Fu,
    HAL_ADC_EXTTRIG_PA1_INT         = 0x10u,
    HAL_ADC_EXTTRIG_PA2_INT         = 0x11u,
    HAL_ADC_EXTTRIG_PA3_INT         = 0x12u,
    HAL_ADC_EXTTRIG_PB4_INT         = 0x13u,
    HAL_ADC_EXTTRIG_PB5_INT         = 0x14u,
    HAL_ADC_EXTTRIG_PA5_INT         = 0x15u,
    HAL_ADC_EXTTRIG_PA6_INT         = 0x16u,
    HAL_ADC_EXTTRIG_PC5_INT         = 0x17u,
    HAL_ADC_EXTTRIG_PC6_INT         = 0x18u,
    HAL_ADC_EXTTRIG_PA7_INT         = 0x19u,
    HAL_ADC_EXTTRIG_PA8_INT         = 0x1Au,
    HAL_ADC_EXTTRIG_PA9_INT         = 0x1Bu,
    HAL_ADC_EXTTRIG_PA10_INT        = 0x1Cu,
    HAL_ADC_EXTTRIG_PB0_INT         = 0x1Du,
    HAL_ADC_EXTTRIG_PB1_INT         = 0x1Eu,
    HAL_ADC_EXTTRIG_PD0_INT         = 0x1Fu,
} HAL_ADC_ExtTrigger_SrcTypeDef;

typedef enum HAL_ADC_SampleShift
{
    HAL_ADC_SAMPLE_SHIFT_0NS_PE     = 0,     /*!< No shift with SYSCLK positive edge    */
    HAL_ADC_SAMPLE_SHIFT_4NS_PE     = 1,     /*!< Shift 4 ns with SYSCLK positive edge  */
    HAL_ADC_SAMPLE_SHIFT_8NS_PE     = 2,     /*!< Shift 8 ns with SYSCLK positive edge  */
    HAL_ADC_SAMPLE_SHIFT_12NS_PE    = 3,     /*!< Shift 12 ns with SYSCLK positive edge */
    HAL_ADC_SAMPLE_SHIFT_0NS_NE     = 4,     /*!< No shift with SYSCLK negative edge    */
    HAL_ADC_SAMPLE_SHIFT_4NS_NE     = 5,     /*!< Shift 4 ns with SYSCLK negative edge  */
    HAL_ADC_SAMPLE_SHIFT_8NS_NE     = 6,     /*!< Shift 8 ns with SYSCLK negative edge  */
    HAL_ADC_SAMPLE_SHIFT_16NS_NE    = 7,     /*!< Shift 12 ns with SYSCLK negative edge */
} HAL_ADC_SampleShiftTypeDef;

#define ADC_SAMPLE_4CYCLE            0
#define ADC_SAMPLE_8CYCLE            1
//=============================================================================
//                  Macro Definition
//=============================================================================
#define __HAL_OPA_ENABLE_CASCADING(a)

#define HAL_Delay(a)        Sleep(a)

#define err(str, ...)       do{ printf("[%s:%d]" str, __func__, __LINE__, ##__VA_ARGS__); /* while(1); */ }while(0)
#define msg(str, ...)       printf(str, ##__VA_ARGS__)
#define trace(str, ...)         printf("[%s:%d] " str, __func__, __LINE__,  ##__VA_ARGS__)

#define FOURCC(a, b, c, d)      ((((a) & 0xFF) << 24) | (((b) & 0xFF) << 16) | (((c) & 0xFF) << 8) | ((d) & 0xFF))

#define info                        printf
#define __BKPT(a)

#define _SystemClock_Config()
#define dac8562_init()              0

#define HAL_Init()
#define LogInit()


#define __HAL_ADC_IS_BUSY(a)        0
//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct multiplexer_pin_cfg
{
    void        *port;
    uint32_t    pin;

} multiplexer_pin_cfg_t;

typedef struct multiplexer_handle
{
    multiplexer_pin_cfg_t  port_s0;
    multiplexer_pin_cfg_t  port_s1;
    multiplexer_pin_cfg_t  port_s2;
} multiplexer_handle_t;

typedef struct OPA_Init
{
    OPA_ModeTypeDef         OP1_Mode;   /*!< Specifies the OP1 mode
                                            This parameter must be a value of @ref OPA_ModeTypeDef */
    OPA_OutTypeDef          OP1_Out;    /*!< Specifies the OP1 output type
                                            This parameter must be a value of @ref OPA_OutTypeDef */

    OPA_ModeTypeDef         OP2_Mode;   /*!< Specifies the OP2 mode (L030 ONLY supports OPA_MODE_STANDALONE)
                                            This parameter must be a value of @ref OPA_ModeTypeDef */

    OPA_OutTypeDef          OP2_Out;    /*!< Specifies the OP2 output type
                                            This parameter must be a value of @ref OPA_OutTypeDef */

    OPA_DAC_VrefTypeDef     Vref;       /*!< Specifies the OP2 mode
                                            This parameter must be a value of @ref OPA_DAC_VrefTypeDef */

    uint8_t                 DC_Level_Step;  /* range: 0 ~ 63 */

} OPA_InitTypeDef;


typedef struct
{
    OPA_TypeDef                 *Instance;      /*!< OPA instance's registers base address   */
    OPA_InitTypeDef             Init;           /*!< OPA required parameters */
//    HAL_StatusTypeDef           Status;         /*!< OPA peripheral status   */
//    HAL_LockTypeDef             Lock;           /*!< Locking object          */
//    __IO HAL_OPA_StateTypeDef   State;          /*!< OPA communication state */

} HAL_OPA_HandleTypeDef;

typedef struct OPA_Config
{
    OPA_IdTypeDef       OP_Id;          /*!< OPA target ID @ref OPA_IdTypeDef     */
    OPA_ModeTypeDef     Mode;           /*!< OPA targer mode @ref OPA_ModeTypeDef */
    OPA_OutTypeDef      OutType;        /*!< OPA target output typt @ref OPA_OutTypeDef */

#define OPA_CONFIG_IGNORE_TAG    -1
    int8_t              DC_Level_Step;  /* range: 0 ~ 63, OPA_CONFIG_SKIP_DC_LEVEL will ignore this parament */
} OPA_ConfigTypeDef;

typedef struct ADC_Init
{
    HAL_ADC_SampleDutyTypeDef   SamplingTime;
    HAL_ADC_ChannelSelTypeDef   AChannelSel;
    HAL_ADC_ClkSelTypeDef       ClkSel;
    HAL_ADC_ConvModeTypeDef     ConvMode;
    HAL_ADC_AutoAccTypeDef      AutoAccumulation;
    HAL_ADC_CircleModeTypeDef   CircleMode;
    HAL_ADC_VrefTypeDef         Vref;
    uint32_t                    NbrOfConversion;
    uint32_t                    ContinueChannelSel;
    HAL_ADC_ExtTrigger_SrcTypeDef   ExtTrigConv0;
    HAL_ADC_ExtTrigger_SrcTypeDef   ExtTrigConv1;
    HAL_ADC_SampleShiftTypeDef      ClkShift;

} ADC_InitTypeDef;


typedef struct
{
    ADC_TypeDef         *Instance;          /*!< Register base address */
    ADC_InitTypeDef     Init;               /*!< ADC required parameters */

//    HAL_LockTypeDef     Lock;               /*!< ADC locking object */
//    __IO uint32_t       State;              /*!< ADC communication state (bitmap of ADC states) */
    // __IO uint32_t       ErrorCode;          /*!< ADC Error code @erf ADC_Error_Code */
} ADC_HandleTypeDef;

//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
static HAL_StatusTypeDef
HAL_OPA_Init(HAL_OPA_HandleTypeDef *hOPA) { return 0; }

static HAL_StatusTypeDef
HAL_OPA_DeInit(HAL_OPA_HandleTypeDef *hOPA) { return 0; }

static HAL_StatusTypeDef
HAL_OPA_Start(HAL_OPA_HandleTypeDef *hOPA, OPA_IdTypeDef OP_Id) { return 0; }

static HAL_StatusTypeDef
HAL_OPA_Stop(HAL_OPA_HandleTypeDef *hOPA, OPA_IdTypeDef OP_Id) { return 0; }

static HAL_StatusTypeDef HAL_OPA_Config(HAL_OPA_HandleTypeDef *hOPA, OPA_ConfigTypeDef *pCfg) { return 0; }

static float ads1256_average_voltage(ads1256_ain_t ain, int times, uint32_t *pCode) { return 2.55f;}

static HAL_StatusTypeDef HAL_ADC_Init(ADC_HandleTypeDef *hADC)   { return 0; }
static HAL_StatusTypeDef HAL_ADC_DeInit(ADC_HandleTypeDef *hADC) { return 0; }

static HAL_StatusTypeDef HAL_ADC_Start(ADC_HandleTypeDef *hADC) { return 0; }
static HAL_StatusTypeDef HAL_ADC_Stop(ADC_HandleTypeDef *hADC)  { return 0; }
static uint32_t HAL_ADC_GetValue(ADC_HandleTypeDef *hADC, HAL_ADC_ChannelSelTypeDef channel) { return 123;}

static int multiplexer_set_channel(multiplexer_handle_t *pHMult, multiplexer_chnnl_t channel_id) { return 0;}

#ifdef __cplusplus
}
#endif

#endif
