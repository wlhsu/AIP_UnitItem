/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file probe.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/04
 * @license
 * @description
 */

#ifndef __probe_H_wi7zI2Y7_lEDS_HgNC_s1DE_uqO2wmue4qrw__
#define __probe_H_wi7zI2Y7_lEDS_HgNC_s1DE_uqO2wmue4qrw__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "err_code.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
#define CONFIG_XFER_MAX_LENGTH          255

typedef enum probe_err
{
    PROBE_ERR_BUFFER_FULL       = ERR_CODE_BUFFER_FULL,
    PROBE_ERR_NO_INCOMING       = ERR_CODE_NO_INCOMING,
    PROBE_ERR_OK                = ERR_CODE_OK,
    PROBE_ERR_NULL_POINTER      = ERR_CODE_NULL_POINTER,
    PROBE_ERR_WRONG_PARAM       = ERR_CODE_WRONG_PARAM,
    PROBE_ERR_UNKNOWN_FAIL      = ERR_CODE_UNKNOWN_FAIL,
    PROBE_ERR_PHY_READ_FAIL     = ERR_CODE_PROBE_READ_FAIL,
    PROBE_ERR_PHY_WRITE_FAIL    = ERR_CODE_PROBE_WRITE_FAIL,
    PROBE_ERR_CHECKSUM_FAIL     = ERR_CODE_PROBE_CHECKSUM_FAIL,
    PROBE_ERR_NO_INSTANCE       = ERR_CODE_PROBE_NO_INSTANCE,
    PROBE_ERR_PHY_INIT_FAIL     = ERR_CODE_PROBE_INIT_FAIL,

} probe_err_t;

typedef enum probe_cmd
{
    PROBE_CMD_NONE              = 0x0,
    PROBE_CMD_PING              = 0x55,
    PROBE_CMD_ACK               = 0xAA,

    PROBE_CMD_REQ_EXEC_OPCODE   = 0x30,

    PROBE_CMD_REQ_REPORT        = 0x01,
    PROBE_CMD_GET_STATUS        = 0x02,

    PROBE_CMD_RESP_REPORT       = 0x10,
} probe_cmd_t;
//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct probe_phy
{
    probe_err_t     (*cb_init)();
    probe_err_t     (*cb_deinit)();

    probe_err_t     (*cb_read)(uint8_t *pData, uint32_t *pLength, void *pExtra);
    probe_err_t     (*cb_write)(uint8_t *pData, uint32_t length, void *pExtra);
} probe_phy_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================
probe_err_t
probe_mst_init(probe_phy_t *pPhy);

probe_err_t
probe_mst_deinit(void);

probe_err_t
probe_mst_read(
    probe_cmd_t *pCmd,
    uint8_t     *pData,
    uint32_t    *pLength,
    void        *pExtra);

probe_err_t
probe_mst_write(
    probe_cmd_t cmd,
    uint8_t     *pData,
    uint32_t    length,
    void        *pExtra);

probe_err_t
probe_slv_init(probe_phy_t *pPhy);

probe_err_t
probe_slv_deinit(void);

probe_err_t
probe_slv_read(
    probe_cmd_t *pCmd,
    uint8_t     *pData,
    uint32_t    *pLength,
    void        *pExtra);

probe_err_t
probe_slv_write(
    probe_cmd_t cmd,
    uint8_t     *pData,
    uint32_t    length,
    void        *pExtra);
#ifdef __cplusplus
}
#endif

#endif
