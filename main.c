#include <stdio.h>
#include <stdlib.h>
//#include <pthread.h>
#include <windows.h>

#include <stdint.h>
#include <stdbool.h>
#include "probe.h"
#include "unit_item.h"

#include "sys_sync.h"
#include "fake_api.h"
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================
#define FOURCC(a, b, c, d)      ((((a) & 0xFF) << 24) | (((b) & 0xFF) << 16) | (((c) & 0xFF) << 8) | ((d) & 0xFF))

#define stringize(s)    #s
#define _toStr(a)       stringize(a)

//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct dut_attr
{
    char            *name;
    uint8_t         i2c_addr;
    uint8_t         has_exist;
} dut_attr_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================
extern int dut_proc(void);

extern int
_console_cmd_probe(
    int                 argc,
    char                **argv,
    void                *pExtra);


static HANDLE           g_hEventDUTReady;
DWORD     g_thid_dut;
DWORD     g_thid_host;

multiplexer_handle_t    g_HMult;

//=============================================================================
//                  Private Function Definition
//=============================================================================
//static void*
//_thread_dut(void* data)
static uint32_t __stdcall
_thread_dut(PVOID pM)
{
    printf(" @ DUT emulator now...\n");

    if( !SetEvent(g_hEventDUTReady) ) //set thread start event
    {
        err("set event fail \n");
    }

    dut_proc();

    return 0;
}

//=============================================================================
//                  Public Function Definition
//=============================================================================
int main(int argc, char **argv)
{
    int         has_exit = false;

    static HANDLE   hTh;

    g_lock = false;

    g_hEventDUTReady = CreateEvent(0, FALSE, FALSE, 0); //create thread start event
    if( g_hEventDUTReady == 0 )
    {
        err("create event fail \n");
    }

    g_thid_host = GetCurrentThreadId();

    hTh = CreateThread(NULL, 0 /* use default stack size */,
                       (LPTHREAD_START_ROUTINE)_thread_dut,
                       (void*)&has_exit, // argument to thread function
                       0,               // use default creation flags
                       &g_thid_dut);    // returns the th
    if( hTh == NULL )
    {
        err("CreateThread fail\n");
    }

    WaitForSingleObject(g_hEventDUTReady, INFINITE);
    Sleep(10);

    printf("  start host\n");
//    while(1)
    {
        #if 1
        char    *pMy_argv[] =
        {
            [0] = "probe",
//            [1] = "help",
            [1] = "--dut",
            [2] = "1",
            [3] = "--dacAStart",
            [4] = "32768",
            [5] = "--dacAGap",
            [6] = "0",
            [7] = "--dacBStart",
            [8] = "32768",
            [9] = "--dacBGap",
            [10] = "0",
            [11] = "--opcode",
            [12] = "0",
            [13] = "--arg1",
            [14] = "1",
            [15] = "--arg2",
            [16] = "2",
            [17] = "--arg3",
            [18] = "7",
            [19] = "--arg4",
            [20] = "1",
            [21] = "--keepMsg",
            [22] = "1",

        };

        int     my_argc = sizeof(pMy_argv)/sizeof(pMy_argv[0]);
        #else
        #define my_argc      argc
        #define pMy_argv     argv
        #endif

        int     i = 0xC000;//0x5000;
//        for(int i = 0x5038; i <= 0x5043; i++)
        {
            char    opcode_num[32] = {0};
            snprintf(opcode_num, 32, "0x%X", i);
            pMy_argv[12] = opcode_num;
            printf("*** %s\n", pMy_argv[12]);

            _console_cmd_probe(my_argc, pMy_argv, 0);
            _console_cmd_probe(my_argc, pMy_argv, 0);
        }
    }


    has_exit = true;

    CloseHandle(hTh);

    system("pause");
    return 0;
}

#if 0

Test OPA
  --arg1 [DC Level]    0 ~ 63
  --arg2 [DAC Vref]    0: Vcap, 1: VDD


AIP_OPCODE_TEST_OPA1_CALIBRATION    = 0x5036
AIP_OPCODE_TEST_OPA1_FOLLOWER_INP   = 0x5037
AIP_OPCODE_TEST_OPA1_INVERTING      = 0x5038
AIP_OPCODE_TEST_OPA1_NONINVERTING   = 0x5039
AIP_OPCODE_TEST_OPA1_DIFF           = 0x503A
AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC   = 0x503B
AIP_OPCODE_TEST_OPA2_CALIBRATION    = 0x503C
AIP_OPCODE_TEST_OPA2_FOLLOWER_INP   = 0x503D
AIP_OPCODE_TEST_OPA2_INVERTING      = 0x503E
AIP_OPCODE_TEST_OPA2_NONINVERTING   = 0x503F
AIP_OPCODE_TEST_OPA2_DIFF           = 0x5040
AIP_OPCODE_TEST_OPA_CASCADING       = 0x5041

Test DAC8562
AIP_OPCODE_TEST_DAC8562       = 0xC000

Test ADC
  --arg1 [Duty Cycle]      0: 4 cycles, 1: 8 cycles
  --arg2 [Vref]            0: Vcap, 1: VDD, 2: External Vref
  --arg3 [CLK Division]    Division 2^x, x = 0 ~ 7. E.g. --arg3 3 => Division 2^3
  --arg4 [CLK Shift]       0: No shift with SYSCLK positive edge
                           1: Shift 4 ns with SYSCLK positive edge
                           2: Shift 8 ns with SYSCLK positive edge
                           3: Shift 12 ns with SYSCLK positive edge
                           4: No shift with SYSCLK negative edge
                           5: Shift 4 ns with SYSCLK negative edge
                           6: Shift 8 ns with SYSCLK negative edge
                           7: Shift 12 ns with SYSCLK negative edge

AIP_OPCODE_TEST_AIN_00              = 0x5000
AIP_OPCODE_TEST_AIN_01              = 0x5001
AIP_OPCODE_TEST_AIN_02              = 0x5002
AIP_OPCODE_TEST_AIN_09              = 0x5006
AIP_OPCODE_TEST_AIN_10              = 0x5007
AIP_OPCODE_TEST_AIN_11              = 0x5008
AIP_OPCODE_TEST_AIN_12              = 0x5009
AIP_OPCODE_TEST_AIN_13              = 0x500A
AIP_OPCODE_TEST_AIN_14              = 0x500B
AIP_OPCODE_TEST_AIN_16              = 0x500C
AIP_OPCODE_TEST_AIN_17              = 0x500D
AIP_OPCODE_TEST_AIN_20              = 0x500E
AIP_OPCODE_TEST_AIN_22              = 0x500F

    .pDescription = "  probe analog IP of DUT\n"
                    "  usage: probe [option] [value] ...\n"
                    "    --devflag [Device ID flag]\n"
                    "       0: HOST\n"
                    "       1: DUT 1\n"
                    "       2: DUT 2\n"
                    "       3: DUT 3\n"
                    "       4: DUT 4\n"
                    "    --dacAStart, --dacBStart [Start input code]\n"
                    "       input code of DAC (0 ~ 65535)\n"
                    "    --dacAGap, --dacBGap [Increased code]\n"
                    "       unit of auto-increasing\n"
                    "       ps. '0' means to probe once\n"
                    "    e.g. probe --dutflag 1 --dacAStart 32768 --dacAGap 0 --opcode 0xC000\n"
                    "         probe --dutflag 1 --dacAStart 32768 --dacAGap 0 --dacBStart 32768 --dacBGap 0 --opcode 0xC000\n\n",
#endif // 0
