/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file unit_item_adc.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/22
 * @license
 * @description
 */

#include <stdlib.h>
#include <string.h>
#include "unit_item.h"
#include "probe.h"

#if defined(CONFIG_SIM) /* Simulation */

#include "fake_api.h"
#include <stdio.h>
#define probe_init                  probe_mst_init
#define probe_deinit                probe_mst_deinit
#define probe_read                  probe_mst_read
#define probe_write                 probe_mst_write


#else
#include "log.h"

#include "main.h"
#include "dac8562.h"
#include "ads1256.h"
#include "mcp47cvb11.h"
#endif // defined
//=============================================================================
//                  Constant Definition
//=============================================================================
#define CFG_DUT_ADC_ACCURATE_BITS       12
//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
extern unit_item_desc_t        g_uitem_adc;

static aip_adc_duty_cycle_t     g_adc_duty_cycle = AIP_ADC_DUTY_CYCLE_4;
static aip_adc_vref_t           g_adc_vref = AIP_ADC_VREF_VCAP;
static aip_adc_clk_div_t        g_adc_clk_div = AIP_ADC_CLK_DIV_1;
static aip_adc_clk_shft_t       g_adc_clk_shft = AIP_ADC_CLK_SHFT_0NS_PE;
//=============================================================================
//                  Private Function Definition
//=============================================================================
static unit_item_err_t
_adc_send_cmd(
    ad_package_t        *pHdr_ad,
    unit_item_cfg_t     *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    probe_err_t         rst = PROBE_ERR_OK;
    uint32_t            len = 0;

    do {
        probe_cmd_t     cmd = PROBE_CMD_NONE;
        rst = probe_write(PROBE_CMD_REQ_EXEC_OPCODE, (uint8_t*)pHdr_ad, sizeof(ad_package_t), (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'verify' fail \n");
            rval = UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL;
            break;
        }

        HAL_Delay(5);
        memset((void*)pCfg->pBuf_pool, 0x0, pCfg->buf_pool_size);

        len = (4 < pCfg->buf_pool_size) ? 4 : pCfg->buf_pool_size;
        rst = probe_read(&cmd, (uint8_t*)pHdr_ad, &len, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK || cmd != PROBE_CMD_ACK )
        {
            err("read 'ACK' fail (return cmd= 0x%02X) \n", cmd);
            rval = UNIT_ITEM_ERR_DUT_COMM_READ_FAIL;
            break;
        }

        /* Get Status */
        rst = probe_write(PROBE_CMD_GET_STATUS, (uint8_t*)pHdr_ad, 1, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'get status' fail \n");
            rval = UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL;
            break;
        }

        HAL_Delay(3);
        memset((void*)pCfg->pBuf_pool, 0x0, pCfg->buf_pool_size);

        len = (sizeof(ad_package_base_t) < pCfg->buf_pool_size) ? sizeof(ad_package_base_t) : pCfg->buf_pool_size;
        rst = probe_read(&cmd, (uint8_t*)pHdr_ad, &len, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK || cmd != PROBE_CMD_ACK )
        {
            err("read 'ACK' fail (return cmd= 0x%02X) \n", cmd);
            rval = UNIT_ITEM_ERR_DUT_COMM_READ_FAIL;
            break;
        }

        rval = (unit_item_err_t)pHdr_ad->base.err_code;

    } while(0);
    return rval;
}


static unit_item_err_t
_adc_usage()
{
    msg("\n%s\n", g_uitem_adc.pDescription);
    msg("  --arg1 [Duty Cycle]      0: 4 cycles, 1: 8 cycles\n"
        "  --arg2 [Vref]            0: Vcap, 1: VDD, 2: External Vref\n"
        "  --arg3 [CLK Division]    Division 2^x, x = 0 ~ 7. E.g. --arg3 3 => Division 2^3 \n");
    msg("  --arg4 [CLK Shift]       0: No shift with SYSCLK positive edge\n"
        "                           1: Shift 4 ns with SYSCLK positive edge\n"
        "                           2: Shift 8 ns with SYSCLK positive edge\n");
    msg("                           3: Shift 12 ns with SYSCLK positive edge\n"
        "                           4: No shift with SYSCLK negative edge\n"
        "                           5: Shift 4 ns with SYSCLK negative edge\n");
    msg("                           6: Shift 8 ns with SYSCLK negative edge\n"
        "                           7: Shift 12 ns with SYSCLK negative edge\n"
        "\n");
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_00), AIP_OPCODE_TEST_AIN_00);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_01), AIP_OPCODE_TEST_AIN_01);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_02), AIP_OPCODE_TEST_AIN_02);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_09), AIP_OPCODE_TEST_AIN_09);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_10), AIP_OPCODE_TEST_AIN_10);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_11), AIP_OPCODE_TEST_AIN_11);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_12), AIP_OPCODE_TEST_AIN_12);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_13), AIP_OPCODE_TEST_AIN_13);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_14), AIP_OPCODE_TEST_AIN_14);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_16), AIP_OPCODE_TEST_AIN_16);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_17), AIP_OPCODE_TEST_AIN_17);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_20), AIP_OPCODE_TEST_AIN_20);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_AIN_22), AIP_OPCODE_TEST_AIN_22);

    return UNIT_ITEM_ERR_OK;
}

/**
 *  @brief  Authenticate the opcode is supported or not
 *
 *  @param [in] opcode          opcode, @ref aip_opcode_t
 *  @return
 *      enunm unit_item_err
 */
static unit_item_err_t
_adc_auth_opcode(aip_opcode_t opcode, unit_item_argv_t *pArgv)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    int                 arg_idx = 0;

    /* Authenticate opcode */
    if( opcode >= AIP_OPCODE_TEST_AIN_00 &&
        opcode <= AIP_OPCODE_TEST_AIN_22 )
        rval = UNIT_ITEM_ERR_GET_UITEM;


    /* Prepare parameters */
    arg_idx = 0;
    g_adc_duty_cycle = (*(pArgv->u.def.argv[arg_idx].pArgv + 1) == 'x')
                     ? (aip_adc_duty_cycle_t)strtol(pArgv->u.def.argv[arg_idx].pArgv + 2, NULL, 16)
                     : (aip_adc_duty_cycle_t)strtol(pArgv->u.def.argv[arg_idx].pArgv, NULL, 10);

    arg_idx++;
    g_adc_vref = (*(pArgv->u.def.argv[arg_idx].pArgv + 1) == 'x')
                ? (aip_adc_vref_t)strtol(pArgv->u.def.argv[arg_idx].pArgv + 2, NULL, 16)
                : (aip_adc_vref_t)strtol(pArgv->u.def.argv[arg_idx].pArgv, NULL, 10);

    arg_idx++;
    g_adc_clk_div = (*(pArgv->u.def.argv[arg_idx].pArgv + 1) == 'x')
                    ? (aip_adc_clk_div_t)strtol(pArgv->u.def.argv[arg_idx].pArgv + 2, NULL, 16)
                    : (aip_adc_clk_div_t)strtol(pArgv->u.def.argv[arg_idx].pArgv, NULL, 10);

    arg_idx++;
    g_adc_clk_shft = (*(pArgv->u.def.argv[arg_idx].pArgv + 1) == 'x')
                    ? (aip_adc_clk_shft_t)strtol(pArgv->u.def.argv[arg_idx].pArgv + 2, NULL, 16)
                    : (aip_adc_clk_shft_t)strtol(pArgv->u.def.argv[arg_idx].pArgv, NULL, 10);

    /* verify parameters */
    do {
        if( g_adc_duty_cycle > (uint32_t)AIP_ADC_DUTY_CYCLE_8 )
        {
            err("duty cycle (%d) out range , support 0 or 1\n", g_adc_duty_cycle);
            rval = UNIT_ITEM_ERR_WRONG_PARAM;
            break;
        }

        if( g_adc_vref > (uint32_t)AIP_ADC_VREF_EXT_VREF )
        {
            err("Vref (%d) out range , support 0 ~ %d\n", g_adc_vref, AIP_ADC_VREF_EXT_VREF);
            rval = UNIT_ITEM_ERR_WRONG_PARAM;
            break;
        }

        if( g_adc_clk_div > (uint32_t)AIP_ADC_CLK_DIV_128 )
        {
            err("CLK Division (%d) out range , support 0 ~ %d\n", g_adc_vref, AIP_ADC_CLK_DIV_128);
            rval = UNIT_ITEM_ERR_WRONG_PARAM;
            break;
        }

        if( g_adc_clk_shft > (uint32_t)AIP_ADC_CLK_SHFT_16NS_NE )
        {
            err("CLK Shift (%d) out range , support 0 ~ %d\n", g_adc_vref, AIP_ADC_CLK_SHFT_16NS_NE);
            rval = UNIT_ITEM_ERR_WRONG_PARAM;
            break;
        }

        msg("duty= %d, Vref= %d, ClkDiv= %d, ClkShft= %d\n",
            g_adc_duty_cycle, g_adc_vref,
            g_adc_clk_div, g_adc_clk_shft);
    } while(0);
    return rval;
}

static unit_item_err_t
_adc_configuate(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    int                 err_code = 0;

    /* set DAC or SPVDD(for Vref selection) */
    if( g_adc_vref == AIP_ADC_VREF_EXT_VREF &&
        pCfg->dac_chB_stage >= 0 )
    {
        err_code = dac8562_set_vout(DAC8562_CHANNEL_B, pCfg->dac_chB_stage);
        if( err_code )
        {
            rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
            err("DAC-Ch B set fail \n");
            return rval;
        }
    }

    switch( pCfg->opcode )
    {
        default:
            break;
        case AIP_OPCODE_TEST_AIN_10:
        case AIP_OPCODE_TEST_AIN_11:
        case AIP_OPCODE_TEST_AIN_12:
        case AIP_OPCODE_TEST_AIN_13:
        case AIP_OPCODE_TEST_AIN_14:
            /* DUT internal cases */
            break;

        case AIP_OPCODE_TEST_AIN_01:
        case AIP_OPCODE_TEST_AIN_02:
        case AIP_OPCODE_TEST_AIN_09:
        case AIP_OPCODE_TEST_AIN_16:
        case AIP_OPCODE_TEST_AIN_22:
            // DAC#B
            if( pCfg->dac_chB_stage >= 0 )
            {
                err_code = dac8562_set_vout(DAC8562_CHANNEL_B, pCfg->dac_chB_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch B set fail \n");
                    break;
                }
            }
            break;

        case AIP_OPCODE_TEST_AIN_00:
        case AIP_OPCODE_TEST_AIN_17:
        case AIP_OPCODE_TEST_AIN_20:
            // DAC#A
            if( pCfg->dac_chA_stage >= 0 )
            {
                // out to PB13 of DUT
                err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch A set fail \n");
                    break;
                }
            }
            break;
    }
    return rval;
}

static unit_item_err_t
_adc_proc(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    ad_package_t        *pHdr_ad = (ad_package_t*)pCfg->pBuf_pool;

    switch( pCfg->opcode )
    {
        default:
            break;
        case AIP_OPCODE_TEST_AIN_10:
        case AIP_OPCODE_TEST_AIN_11:
        case AIP_OPCODE_TEST_AIN_12:
        case AIP_OPCODE_TEST_AIN_13:
        case AIP_OPCODE_TEST_AIN_14:
            /* DUT internal*/
//            break;

        case AIP_OPCODE_TEST_AIN_01:
        case AIP_OPCODE_TEST_AIN_02:
        case AIP_OPCODE_TEST_AIN_09:
        case AIP_OPCODE_TEST_AIN_16:
        case AIP_OPCODE_TEST_AIN_22:
            /* DAC#B */
//            break;

        case AIP_OPCODE_TEST_AIN_00:
        case AIP_OPCODE_TEST_AIN_17:
        case AIP_OPCODE_TEST_AIN_20:
            /* DAC#A */
            #if 0
            pHdr_ad->base.opcode    = AIP_OPCODE_TEST_ADC_OPEN;
            pHdr_ad->base.err_code  = AIP_ERR_OK;

            pHdr_ad->u.adc.cfg.duty_cycle = g_adc_duty_cycle;
            pHdr_ad->u.adc.cfg.vref       = g_adc_vref;
            pHdr_ad->u.adc.cfg.clk_div    = g_adc_clk_div;
            pHdr_ad->u.adc.cfg.clk_shift  = g_adc_clk_shft;

            rval = _adc_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
            {
                err("ADC open fail\n");
                break;
            }
            #endif

            pHdr_ad->base.opcode      = pCfg->opcode;
            pHdr_ad->base.err_code    = AIP_ERR_OK;

            pHdr_ad->u.adc.cfg.duty_cycle = g_adc_duty_cycle;
            pHdr_ad->u.adc.cfg.vref       = g_adc_vref;
            pHdr_ad->u.adc.cfg.clk_div    = g_adc_clk_div;
            pHdr_ad->u.adc.cfg.clk_shift  = g_adc_clk_shft;

            rval = _adc_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
            {
                err("ADC open fail\n");
                break;
            }
            break;
    }
    return rval;
}


static unit_item_err_t
_adc_analyze_result(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    float               voltage_a = 0.0f;
    float               voltage_b = 0.0f;
    unit_item_result_t  *pSlot_cur = 0;

    if( !pCfg->pUItem_results )
    {
        msg("@DAC8562 ");
        if( pCfg->dac_chA_stage >= 0 )
        {
            voltage_a = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            msg("#A(code= %5d)= %2.6f, ", pCfg->dac_chA_stage, voltage_a);
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            voltage_b = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            msg("#B(code= %5d)= %2.6f, ", pCfg->dac_chB_stage, voltage_b);
        }
    }
    else
    {
        pSlot_cur = &pCfg->pUItem_results[pCfg->uitem_rst_cnt];
        if( pCfg->dac_chA_stage >= 0 )
        {
            pSlot_cur->dacA_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            pSlot_cur->dacA_code     = pCfg->dac_chA_stage;
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            pSlot_cur->dacB_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);;
            pSlot_cur->dacB_code     = pCfg->dac_chB_stage;
        }
    }

#if 0
    switch( pCfg->opcode )
    {
        default:
            break;
        case AIP_OPCODE_TEST_AIN_10:
        case AIP_OPCODE_TEST_AIN_11:
        case AIP_OPCODE_TEST_AIN_12:
        case AIP_OPCODE_TEST_AIN_13:
        case AIP_OPCODE_TEST_AIN_14:
            /* DUT internal*/
           break;

        case AIP_OPCODE_TEST_AIN_01:
        case AIP_OPCODE_TEST_AIN_02:
        case AIP_OPCODE_TEST_AIN_09:
        case AIP_OPCODE_TEST_AIN_16:
        case AIP_OPCODE_TEST_AIN_22:
            /* DAC#B */
           break;

        case AIP_OPCODE_TEST_AIN_00:
        case AIP_OPCODE_TEST_AIN_17:
        case AIP_OPCODE_TEST_AIN_20:
            /* DAC#A */
            break;
    }
#endif

    do {
        probe_err_t         rst = PROBE_ERR_OK;
        probe_cmd_t         cmd = PROBE_CMD_NONE;
        uint32_t            len = 0;
        double              voltage_ref = 0.0f;
        ad_package_t        *pHdr_ad = (ad_package_t*)pCfg->pBuf_pool;

        pHdr_ad->base.opcode      = pCfg->opcode;
        pHdr_ad->base.err_code    = AIP_ERR_OK;
        pHdr_ad->u.adc.conv_value = 0;

        rst = probe_write(PROBE_CMD_REQ_REPORT, (uint8_t*)pHdr_ad, sizeof(ad_package_t), (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'verify' fail \n");
            rval = UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL;
            break;
        }

        HAL_Delay(2);
        memset((void*)pCfg->pBuf_pool, 0x0, pCfg->buf_pool_size);

        len = (sizeof(ad_package_t) < pCfg->buf_pool_size) ? sizeof(ad_package_t) : pCfg->buf_pool_size;
        rst = probe_read(&cmd, (uint8_t*)pHdr_ad, &len, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK || cmd != PROBE_CMD_ACK )
        {
            err("read 'ACK' fail (return cmd= 0x%02X) \n", cmd);
            rval = UNIT_ITEM_ERR_DUT_COMM_READ_FAIL;
            break;
        }

        if( g_adc_vref == AIP_ADC_VREF_VCAP )
            voltage_ref = (double)pCfg->vcap;
        else if( g_adc_vref == AIP_ADC_VREF_VDD )
        {
            /* SPVDD */
            voltage_ref = ads1256_average_voltage(CONFIG_ADS1256_SPVDD_AIN, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            voltage_ref *= 2;
        }
        else if( g_adc_vref == AIP_ADC_VREF_EXT_VREF )
        {
            /* DAC#B links to PB01 of DUT */
            voltage_ref = (double)voltage_b;
        }

        if( !pSlot_cur )
        {
            msg("@ADC (ConvValue= %4d)= %2.6f, #Vref= %2.6f mV\n",
                pHdr_ad->u.adc.conv_value,
                voltage_ref * pHdr_ad->u.adc.conv_value / (0x1 << CFG_DUT_ADC_ACCURATE_BITS),
                voltage_ref);
        }
        else
        {
            pSlot_cur->adc.conv_value = pHdr_ad->u.adc.conv_value;
            pSlot_cur->vref           = voltage_ref;
        }

        /* terminate ADC */
        pHdr_ad->base.opcode      = AIP_OPCODE_TEST_ADC_CLOSE;
        pHdr_ad->base.err_code    = AIP_ERR_OK;
        rval = _adc_send_cmd(pHdr_ad, pCfg);
        if( rval != UNIT_ITEM_ERR_OK )
        {
            err("ADC close fail\n");
            break;
        }

        pCfg->uitem_rst_cnt++;
    } while(0);
    return rval;
}

static unit_item_err_t
_adc_log_data(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;

    do {
        if( !pCfg->pUItem_results ||
            pCfg->uitem_rst_cnt != pCfg->uitem_rst_slot_size )
            break;

        for(int i = 0; i < pCfg->uitem_rst_slot_size; i++)
        {
            unit_item_result_t  *pSlot_cur = 0;

            pSlot_cur = (unit_item_result_t*)&pCfg->pUItem_results[i];

            // dump log messages
            msg("@DUT #%d Vcap %4.6f, "
                "@DAC8562 #A(code= %5d)= %4.6f, #B(code= %5d)= %4.6f, "
                "@ADC (ConvValue= %4d)= %4.6f, #Vref= %4.6f mV\n",
                pCfg->dut_id + 1, pSlot_cur->vcap,
                pSlot_cur->dacA_code, pSlot_cur->dacA_mVoltage,
                pSlot_cur->dacB_code, pSlot_cur->dacB_mVoltage,
                pSlot_cur->adc.conv_value, pSlot_cur->vref * pSlot_cur->adc.conv_value / (0x1 << CFG_DUT_ADC_ACCURATE_BITS),
                pSlot_cur->vref);
        }

        pCfg->uitem_rst_cnt = 0; // reflash

    } while(0);

    return rval;
}

static void
_adc_delay()
{
    HAL_Delay(50);
    return;
}
//=============================================================================
//                  Public Function Definition
//=============================================================================
unit_item_desc_t        g_uitem_adc =
{
    .cb_auth_opcode     = _adc_auth_opcode,
    .cb_configuate      = _adc_configuate,
    .cb_proc            = _adc_proc,
    .cb_analyze_result  = _adc_analyze_result,
    .cb_usage           = _adc_usage,
    .cb_log_data        = _adc_log_data,
    .cb_delay           = _adc_delay,
    .pDescription       = "Test ADC",
};
