/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file unit_item_template.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/22
 * @license
 * @description
 */


#include "unit_item.h"

//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
extern unit_item_desc_t        g_uitem_xxx;
//=============================================================================
//                  Private Function Definition
//=============================================================================
static unit_item_err_t
_xxx_usage()
{
    printf("\n%s\n", g_uitem_xxx.pDescription);

    return UNIT_ITEM_ERR_OK;
}

static unit_item_err_t
_xxx_auth_opcode(ad_opcode_t opcode)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;

    rval = UNIT_ITEM_ERR_GET_UITEM;

    return rval;
}

static unit_item_err_t
_xxx_configuate(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    /* set DAC */
    return rval;
}

static unit_item_err_t
_xxx_proc(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    /* Trigger verification */
    return rval;
}


static unit_item_err_t
_xxx_analyze_result(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    return rval;
}
//=============================================================================
//                  Public Function Definition
//=============================================================================
unit_item_desc_t        g_uitem_xxx =
{
    .cb_auth_opcode     = _xxx_auth_opcode,
    .cb_configuate      = _xxx_configuate,
    .cb_proc            = _xxx_proc,
    .cb_analyze_result  = _xxx_analyze_result,
    .cb_usage           = _xxx_usage,
    .pDescription       = "Test XXX",
};
