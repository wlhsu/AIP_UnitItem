/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file unit_item_dac.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/22
 * @license
 * @description
 */


#include <stdio.h>
#include "unit_item.h"

#if defined(CONFIG_SIM) /* Simulation */
#include "fake_api.h"

#else

#include "log.h"
#include "main.h"
#include "dac8562.h"
#include "ads1256.h"
#endif
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
extern unit_item_desc_t        g_uitem_dac8562;
//=============================================================================
//                  Private Function Definition
//=============================================================================
static unit_item_err_t
_dac8562_usage()
{
    msg("\n%s\n", g_uitem_dac8562.pDescription);
    msg("%-30s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_DAC8562), AIP_OPCODE_TEST_DAC8562);

    return UNIT_ITEM_ERR_OK;
}

static unit_item_err_t
_dac8562_auth_opcode(aip_opcode_t opcode, unit_item_argv_t *pArgv)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;

    if( opcode == AIP_OPCODE_TEST_DAC8562 )
        rval = UNIT_ITEM_ERR_GET_UITEM;

    return rval;
}

static unit_item_err_t
_dac8562_configuate(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    int                 err_code = 0;

    /* set DAC */
    if( pCfg->dac_chA_stage >= 0 )
    {
        err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
        if( err_code )
        {
            rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
        }
    }

    if( pCfg->dac_chB_stage >= 0 )
    {
        err_code = dac8562_set_vout(DAC8562_CHANNEL_B, pCfg->dac_chB_stage);
        if( err_code )
        {
            rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
        }
    }

    return rval;
}

static unit_item_err_t
_dac8562_proc(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    /* Trigger verification */
    return rval;
}


static unit_item_err_t
_dac8562_analyze_result(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    float               voltage = 0.0f;
    uint32_t            code = 0;
    unit_item_result_t  *pSlot_cur = 0;

    if( !pCfg->pUItem_results )
    {
        msg("DAC8562 Out: ");
        if( pCfg->dac_chA_stage >= 0 )
        {
            voltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, &code);
            msg("#A(code= %d)= %2.6f, code= 0x%08X, ", pCfg->dac_chA_stage, voltage, code);
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            voltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, &code);
            msg("#B(code= %d)= %2.6f, code= 0x%08X", pCfg->dac_chB_stage, voltage, code);
        }

        msg("\n");
    }
    else
    {
        pSlot_cur = &pCfg->pUItem_results[pCfg->uitem_rst_cnt];
        if( pCfg->dac_chA_stage >= 0 )
        {
            pSlot_cur->dacA_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, &code);
            pSlot_cur->dacA_code     = pCfg->dac_chA_stage;

            pSlot_cur->dac8562.chA_ads1256_code = code;
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            pSlot_cur->dacB_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, &code);
            pSlot_cur->dacB_code     = pCfg->dac_chB_stage;

            pSlot_cur->dac8562.chB_ads1256_code = code;
        }
    }

    pCfg->uitem_rst_cnt++;

    return rval;
}

static unit_item_err_t
_dac8562_log_data(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;

    do {
        if( !pCfg->pUItem_results ||
            pCfg->uitem_rst_cnt != pCfg->uitem_rst_slot_size )
            break;

        // dump log messages
        for(int i = 0; i < pCfg->uitem_rst_slot_size; i++)
        {
            unit_item_result_t  *pSlot_cur = 0;

            pSlot_cur = (unit_item_result_t*)&pCfg->pUItem_results[i];

            msg("@DUT #%d Vcap %4.6f, "
                "DAC8562 Out: #A(code= %5d)= %4.6f, code= 0x%08X, #B(code= %5d)= %4.6f, code= 0x%08X\n",
                pCfg->dut_id + 1, pSlot_cur->vcap,
                pSlot_cur->dacA_code, pSlot_cur->dacA_mVoltage, pSlot_cur->dac8562.chA_ads1256_code,
                pSlot_cur->dacB_code, pSlot_cur->dacB_mVoltage, pSlot_cur->dac8562.chB_ads1256_code);
        }

        pCfg->uitem_rst_cnt = 0; // reflash

    } while(0);

    return rval;
}
//=============================================================================
//                  Public Function Definition
//=============================================================================
unit_item_desc_t        g_uitem_dac8562 =
{
    .cb_auth_opcode     = _dac8562_auth_opcode,
    .cb_configuate      = _dac8562_configuate,
    .cb_proc            = 0, //_dac8562_proc,
    .cb_analyze_result  = _dac8562_analyze_result,
    .cb_usage           = _dac8562_usage,
    .cb_log_data        = _dac8562_log_data,
    .pDescription       = "Test DAC8562",
};
