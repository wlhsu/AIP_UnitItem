/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file unit_item_opa.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/22
 * @license
 * @description
 */


#include <stdlib.h>
#include <string.h>
#include "unit_item.h"
#include "probe.h"

#if defined(CONFIG_SIM) /* Simulation */

#include "fake_api.h"

#define probe_init                  probe_mst_init
#define probe_deinit                probe_mst_deinit
#define probe_read                  probe_mst_read
#define probe_write                 probe_mst_write

#else

#include "log.h"

#include "main.h"
#include "dac8562.h"
#include "ads1256.h"

#include "74HC4051.h"
#endif // defined
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================
extern unit_item_desc_t        g_uitem_opa;
extern multiplexer_handle_t    g_HMult;

static uint32_t             g_dc_level = 0;
static aip_opa_dac_vref_t   g_vref __attribute__((used)) = AIP_OPA_DAC_VREF_VCAP;
//=============================================================================
//                  Private Function Definition
//=============================================================================
static unit_item_err_t
_opa_send_cmd(
    ad_package_t        *pHdr_ad,
    unit_item_cfg_t     *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    probe_err_t         rst = PROBE_ERR_OK;
    uint32_t            len = 0;

    do {
        probe_cmd_t     cmd = PROBE_CMD_NONE;
        rst = probe_write(PROBE_CMD_REQ_EXEC_OPCODE, (uint8_t*)pHdr_ad, sizeof(ad_package_t), (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'verify' fail \n");
            rval = UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL;
            break;
        }

        HAL_Delay(5);
        memset((void*)pCfg->pBuf_pool, 0x0, pCfg->buf_pool_size);

        len = (4 < pCfg->buf_pool_size) ? 4 : pCfg->buf_pool_size;
        rst = probe_read(&cmd, (uint8_t*)pHdr_ad, &len, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK || cmd != PROBE_CMD_ACK )
        {
            err("read 'ACK' fail (return cmd= 0x%02X) \n", cmd);
            rval = UNIT_ITEM_ERR_DUT_COMM_READ_FAIL;
            break;
        }

        /* Get Status */
        rst = probe_write(PROBE_CMD_GET_STATUS, (uint8_t*)pHdr_ad, 1, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'get status' fail \n");
            rval = UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL;
            break;
        }

        HAL_Delay(3);
        memset((void*)pCfg->pBuf_pool, 0x0, pCfg->buf_pool_size);

        len = (sizeof(ad_package_base_t) < pCfg->buf_pool_size) ? sizeof(ad_package_base_t) : pCfg->buf_pool_size;
        rst = probe_read(&cmd, (uint8_t*)pHdr_ad, &len, (void*)&pCfg->dut_id);
        if( rst != PROBE_ERR_OK || cmd != PROBE_CMD_ACK )
        {
            err("read 'ACK' fail (return cmd= 0x%02X) \n", cmd);
            rval = UNIT_ITEM_ERR_DUT_COMM_READ_FAIL;
            break;
        }

        rval = (unit_item_err_t)pHdr_ad->base.err_code;

    } while(0);
    return rval;
}


static unit_item_err_t
_opa_usage()
{
    msg("\n%s\n", g_uitem_opa.pDescription);
    msg("  --arg1 [DC Level]    0 ~ 63\n"
        "  --arg2 [DAC Vref]    0: Vcap, 1: VDD\n"
        "\n\n");
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_CALIBRATION ), AIP_OPCODE_TEST_OPA1_CALIBRATION );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_FOLLOWER_INP), AIP_OPCODE_TEST_OPA1_FOLLOWER_INP);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_INVERTING   ), AIP_OPCODE_TEST_OPA1_INVERTING   );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_NONINVERTING), AIP_OPCODE_TEST_OPA1_NONINVERTING);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_DIFF        ), AIP_OPCODE_TEST_OPA1_DIFF        );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC), AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA2_CALIBRATION ), AIP_OPCODE_TEST_OPA2_CALIBRATION );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA2_FOLLOWER_INP), AIP_OPCODE_TEST_OPA2_FOLLOWER_INP);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA2_INVERTING   ), AIP_OPCODE_TEST_OPA2_INVERTING   );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA2_NONINVERTING), AIP_OPCODE_TEST_OPA2_NONINVERTING);
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA2_DIFF        ), AIP_OPCODE_TEST_OPA2_DIFF        );
    msg("%-36s= 0x%04X\n", _toStr(AIP_OPCODE_TEST_OPA_CASCADING    ), AIP_OPCODE_TEST_OPA_CASCADING    );

    return UNIT_ITEM_ERR_OK;
}

/**
 *  @brief  Authenticate the opcode is supported or not
 *
 *  @param [in] opcode          opcode, @ref aip_opcode_t
 *  @return
 *      enunm unit_item_err
 */
static unit_item_err_t
_opa_auth_opcode(aip_opcode_t opcode, unit_item_argv_t *pArgv)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    int                 arg_idx = 0;

    /* Authenticate opcode */
    if( (opcode >= AIP_OPCODE_TEST_OPA1_CALIBRATION &&
         opcode <= AIP_OPCODE_TEST_OPA1_DIFF) ||
        (opcode >= AIP_OPCODE_TEST_OPA2_CALIBRATION &&
         opcode <= AIP_OPCODE_TEST_OPA2_DIFF)
        || opcode == AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC
        || opcode == AIP_OPCODE_TEST_OPA_CASCADING
       #if 0
       || opcode == AIP_OPCODE_TEST_OPA_ALL
       #endif
      )
        rval = UNIT_ITEM_ERR_GET_UITEM;

    /* Prepare parameters */
    arg_idx = 0;
    g_dc_level = (*(pArgv->u.def.argv[arg_idx].pArgv + 1) == 'x')
                ? strtol(pArgv->u.def.argv[arg_idx].pArgv + 2, NULL, 16)
                : strtol(pArgv->u.def.argv[arg_idx].pArgv, NULL, 10);

    arg_idx++;
    g_vref = (pArgv->u.def.argv[arg_idx].pArgv &&
              *pArgv->u.def.argv[arg_idx].pArgv != 0)
           ? AIP_OPA_DAC_VREF_VDD : AIP_OPA_DAC_VREF_VCAP;

    do {
        if( g_dc_level > 63 )
        {
            err("DC level (%d) out range , support 0 ~ 63\n", g_dc_level);
            rval = UNIT_ITEM_ERR_WRONG_PARAM;
            break;
        }
    } while(0);

    return rval;
}

static unit_item_err_t
_opa_configuate(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    int                 err_code = 0;

    /* set DAC */
    switch( pCfg->opcode )
    {
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC:
            msg("No need DAC output\n");
        default:
            break;

        case AIP_OPCODE_TEST_OPA1_CALIBRATION:
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA1_NONINVERTING:
            if( pCfg->dac_chA_stage >= 0 )
            {
                // out to PB13 of DUT
                err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch A set fail \n");
                    break;
                }
            }
            break;

        case AIP_OPCODE_TEST_OPA1_DIFF:
            if( pCfg->dac_chA_stage >= 0 )
            {
                err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch A set fail \n");
                    break;
                }
            }

            if( pCfg->dac_chB_stage >= 0 )
            {
                err_code = dac8562_set_vout(DAC8562_CHANNEL_B, pCfg->dac_chB_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch B set fail \n");
                    break;
                }
            }
            break;

        case AIP_OPCODE_TEST_OPA1_INVERTING:
            break;

        case AIP_OPCODE_TEST_OPA2_CALIBRATION:
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA2_NONINVERTING:
            if( pCfg->dac_chA_stage >= 0 )
            {
                // out to PB2 of DUT
                err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch A set fail \n");
                    break;
                }
            }
            break;

        case AIP_OPCODE_TEST_OPA2_DIFF:
            if( pCfg->dac_chA_stage >= 0 )
            {
                err_code = dac8562_set_vout(DAC8562_CHANNEL_A, pCfg->dac_chA_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch A set fail \n");
                    break;
                }
            }

            if( pCfg->dac_chB_stage >= 0 )
            {
                err_code = dac8562_set_vout(DAC8562_CHANNEL_B, pCfg->dac_chB_stage);
                if( err_code )
                {
                    rval = UNIT_ITEM_ERR_SET_DAC8562_FAIL;
                    err("DAC-Ch B set fail \n");
                    break;
                }
            }
            break;

        case AIP_OPCODE_TEST_OPA2_INVERTING:
            break;
        case AIP_OPCODE_TEST_OPA_CASCADING:
            break;
    }
    return rval;
}

static unit_item_err_t
_opa_proc(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    ad_package_t        *pHdr_ad = (ad_package_t*)pCfg->pBuf_pool;

    /* Trigger verification */
    switch( pCfg->opcode )
    {
        default:    break;
        case AIP_OPCODE_TEST_OPA1_CALIBRATION:
        case AIP_OPCODE_TEST_OPA1_DIFF:
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC:
            pHdr_ad->base.opcode    = AIP_OPCODE_TEST_OPA1_OPEN;
            pHdr_ad->base.err_code  = AIP_ERR_OK;
            pHdr_ad->u.opa.dc_level = g_dc_level;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
            {
                err("open fail\n");
                break;
            }

            pHdr_ad->base.opcode    = pCfg->opcode;
            pHdr_ad->base.err_code  = AIP_ERR_OK;
            pHdr_ad->u.opa.dc_level = g_dc_level;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
            {
                err("exec fail\n");
                break;
            }

            break;

        case AIP_OPCODE_TEST_OPA1_NONINVERTING:
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_INP:
            // VIP input from DAC8562
            pHdr_ad->base.opcode   = AIP_OPCODE_TEST_OPA1_OPEN;
            pHdr_ad->base.err_code = AIP_ERR_OK;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
                break;

            pHdr_ad->base.opcode   = pCfg->opcode;
            pHdr_ad->base.err_code = AIP_ERR_OK;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
                break;

            break;

        case AIP_OPCODE_TEST_OPA2_CALIBRATION:
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA2_NONINVERTING:
        case AIP_OPCODE_TEST_OPA2_DIFF:
            // VIP input from DAC8562
            pHdr_ad->base.opcode   = AIP_OPCODE_TEST_OPA2_OPEN;
            pHdr_ad->base.err_code = AIP_ERR_OK;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
                break;

            pHdr_ad->base.opcode   = pCfg->opcode;
            pHdr_ad->base.err_code = AIP_ERR_OK;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
                break;

            break;

        case AIP_OPCODE_TEST_OPA1_INVERTING:
        case AIP_OPCODE_TEST_OPA2_INVERTING:
            break;

        case AIP_OPCODE_TEST_OPA_CASCADING:
            break;
    }
    return rval;
}


static unit_item_err_t
_opa_analyze_result(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;
    float               voltage_a = 0.0f;
    float               voltage_b = 0.0f;
    unit_item_result_t  *pSlot_cur = 0;

    /* Measure OPA1 output */
    if( !pCfg->pUItem_results )
    {
        msg("@DCLv %d, ", g_dc_level);

        msg("@DAC8562 ");
        if( pCfg->dac_chA_stage >= 0 )
        {
            voltage_a = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            msg("#A(code= %5d)= %4.6f, ", pCfg->dac_chA_stage, voltage_a);
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            voltage_b = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            msg("#B(code= %5d)= %4.6f, ", pCfg->dac_chB_stage, voltage_b);
        }
    }
    else
    {
        pSlot_cur = &pCfg->pUItem_results[pCfg->uitem_rst_cnt];

        pSlot_cur->opa.dc_level = g_dc_level;

        if( pCfg->dac_chA_stage >= 0 )
        {
            pSlot_cur->dacA_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO1_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            pSlot_cur->dacA_code     = pCfg->dac_chA_stage;
        }

        if( pCfg->dac_chB_stage >= 0 )
        {
            pSlot_cur->dacB_mVoltage = ads1256_average_voltage(CONFIG_ADS1256_VDACO2_2, CONIFG_ADS1256_SAMPLE_TIMES, 0);;
            pSlot_cur->dacB_code     = pCfg->dac_chB_stage;
        }
    }

    switch( pCfg->opcode )
    {
        default:
            break;

        case AIP_OPCODE_TEST_OPA1_DIFF:
            if( !pSlot_cur )
            {
                msg("@Diff %4.6f, ", voltage_a - voltage_b);
            }
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC:
        case AIP_OPCODE_TEST_OPA1_CALIBRATION:
        case AIP_OPCODE_TEST_OPA1_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA1_INVERTING:
        case AIP_OPCODE_TEST_OPA1_NONINVERTING:
        case AIP_OPCODE_TEST_OPA2_CALIBRATION:
        case AIP_OPCODE_TEST_OPA2_FOLLOWER_INP:
        case AIP_OPCODE_TEST_OPA2_INVERTING:
        case AIP_OPCODE_TEST_OPA2_NONINVERTING:
        case AIP_OPCODE_TEST_OPA2_DIFF:
        case AIP_OPCODE_TEST_OPA_CASCADING:
            voltage_a = ads1256_average_voltage(CONFIG_ADS1256_OPA_AIN, CONIFG_ADS1256_SAMPLE_TIMES, 0);
            if( !pSlot_cur )
            {
                msg("@OP_O %4.6f mV\n", voltage_a);
            }
            else
            {
                pSlot_cur->opa.opa_o_mVoltage = voltage_a;
            }

            break;
    }

    if( !pSlot_cur )
    {
        msg("\n");
    }

    do { /* terminate OPA */
        ad_package_t        *pHdr_ad = (ad_package_t*)pCfg->pBuf_pool;

        if( pCfg->opcode >= AIP_OPCODE_TEST_OPA1_CALIBRATION &&
            pCfg->opcode <= AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC )
            pHdr_ad->base.opcode = AIP_OPCODE_TEST_OPA1_CLOSE;
        else if( pCfg->opcode >= AIP_OPCODE_TEST_OPA2_CALIBRATION &&
                 pCfg->opcode <= AIP_OPCODE_TEST_OPA2_DIFF )
                 pHdr_ad->base.opcode = AIP_OPCODE_TEST_OPA2_CLOSE;
        else if( pCfg->opcode == AIP_OPCODE_TEST_OPA_CASCADING )
        {
            pHdr_ad->base.opcode = AIP_OPCODE_TEST_OPA1_CLOSE;
            pHdr_ad->base.err_code = AIP_ERR_OK;
            rval = _opa_send_cmd(pHdr_ad, pCfg);
            if( rval != UNIT_ITEM_ERR_OK )
                break;

            pHdr_ad->base.opcode = AIP_OPCODE_TEST_OPA2_CLOSE;
        }
        else    break;

        pHdr_ad->base.err_code = AIP_ERR_OK;
        rval = _opa_send_cmd(pHdr_ad, pCfg);
        if( rval != UNIT_ITEM_ERR_OK )
            break;

        pCfg->uitem_rst_cnt++;
    } while(0);


    return rval;
}

static unit_item_err_t
_opa_log_data(unit_item_cfg_t *pCfg)
{
    unit_item_err_t     rval = UNIT_ITEM_ERR_OK;

    do {
        if( !pCfg->pUItem_results ||
            pCfg->uitem_rst_cnt != pCfg->uitem_rst_slot_size )
            break;

        // ToDo: dump log messages
        for(int i = 0; i < pCfg->uitem_rst_slot_size; i++)
        {
            unit_item_result_t  *pSlot_cur = 0;

            pSlot_cur = (unit_item_result_t*)&pCfg->pUItem_results[i];

            if( pCfg->opcode == AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC )
            {
                msg("@DUT #%d Vcap %4.6f, "
                    "@DCLv %d, @DAC8562 #A(code= %5d)= %4.6f, @OP_O %4.6f mV\n",
                    pCfg->dut_id + 1, pSlot_cur->vcap,
                    pSlot_cur->opa.dc_level,
                    pSlot_cur->dacA_code, pSlot_cur->dacA_mVoltage,
                    pSlot_cur->opa.opa_o_mVoltage);
            }
            else if( pCfg->opcode == AIP_OPCODE_TEST_OPA1_DIFF )
            {
                msg("@DUT #%d Vcap %4.6f, "
                    "@DCLv %d, "
                    "@DAC8562 #A(code= %5d)= %4.6f, #B(code= %5d)= %4.6f, "
                    "@Diff %4.6f, "
                    "@OP_O %4.6f mV\n",
                    pCfg->dut_id + 1, pSlot_cur->vcap,
                    pSlot_cur->opa.dc_level,
                    pSlot_cur->dacA_code, pSlot_cur->dacA_mVoltage,
                    pSlot_cur->dacB_code, pSlot_cur->dacB_mVoltage,
                    pSlot_cur->dacA_mVoltage - pSlot_cur->dacB_mVoltage,
                    pSlot_cur->opa.opa_o_mVoltage);
            }
            else
            {
                msg("@DUT #%d Vcap %4.6f, "
                    "@DCLv %d, "
                    "@DAC8562 #A(code= %5d)= %4.6f, #B(code= %5d)= %4.6f, "
                    "@OP_O %4.6f mV\n",
                    pCfg->dut_id + 1, pSlot_cur->vcap,
                    pSlot_cur->opa.dc_level,
                    pSlot_cur->dacA_code, pSlot_cur->dacA_mVoltage,
                    pSlot_cur->dacB_code, pSlot_cur->dacB_mVoltage,
                    pSlot_cur->opa.opa_o_mVoltage);
            }
        }

        pCfg->uitem_rst_cnt = 0; // reflash

    } while(0);

    return rval;
}
//=============================================================================
//                  Public Function Definition
//=============================================================================
unit_item_desc_t        g_uitem_opa =
{
    .cb_auth_opcode     = _opa_auth_opcode,
    .cb_configuate      = _opa_configuate,
    .cb_proc            = _opa_proc,
    .cb_analyze_result  = _opa_analyze_result,
    .cb_usage           = _opa_usage,
    .cb_log_data        = _opa_log_data,
    .pDescription       = "Test OPA",
};

