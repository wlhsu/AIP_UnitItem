/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file unit_item.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/03/22
 * @license
 * @description
 */

#ifndef __unit_item_H_w28ir5tT_l4bS_HAg1_s6lb_uDbTgsyJBmZb__
#define __unit_item_H_w28ir5tT_l4bS_HAg1_s6lb_uDbTgsyJBmZb__

#ifdef __cplusplus
extern "C" {
#endif

#include "aip_package.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
typedef enum unit_item_err
{
    UNIT_ITEM_ERR_GET_UITEM             = ERR_CODE_GET_UITEM,
    UNIT_ITEM_ERR_OK                    = ERR_CODE_OK,
    UNIT_ITEM_ERR_UNKNOWN_FAIL          = ERR_CODE_UNKNOWN_FAIL,
    UNIT_ITEM_ERR_DUT_COMM_READ_FAIL    = ERR_CODE_PROBE_READ_FAIL,
    UNIT_ITEM_ERR_DUT_COMM_WIRTE_FAIL   = ERR_CODE_PROBE_WRITE_FAIL,
    UNIT_ITEM_ERR_WRONG_PARAM           = ERR_CODE_WRONG_PARAM,
    UNIT_ITEM_ERR_SET_DAC8562_FAIL      = ERR_CODE_SET_DAC8562_FAIL,
    UNIT_ITEM_ERR_OPCODE_EXEC_FAIL      = ERR_CODE_OPCODE_EXEC_FAIL,

} unit_item_err_t;

//=============================================================================
//                  Macro Definition
//=============================================================================
#define stringize(s)    #s
#define _toStr(a)       stringize(a)
//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct unit_item_result
{
#define CONFIG_UNIT_ITEM_RESULT_MAX       20    // the max reserved unit item result data
    float       vcap;
    float       vref;
    float       dacA_mVoltage;
    int         dacA_code;
    float       dacB_mVoltage;
    int         dacB_code;

    union {
        struct {
            float       opa_o_mVoltage;
            uint8_t     dc_level;
        } opa;

        struct {
            uint32_t    chA_ads1256_code;
            uint32_t    chB_ads1256_code;
        } dac8562;

        struct {
            uint16_t    conv_value;
        } adc;
    };
} unit_item_result_t;

typedef struct unit_item_cfg
{
    aip_opcode_t    opcode;
    int             dut_id;

    uint8_t         *pBuf_pool;
    int             buf_pool_size;

    float           vcap;

    int             dac_chA_stage;
    int             dac_chB_stage;

    unit_item_result_t  *pUItem_results;
    uint16_t            uitem_rst_cnt;
    uint16_t            uitem_rst_slot_size;


    void            *pPrivate;
} unit_item_cfg_t;

typedef struct def_argv
{
    union {
        char        *pArgv;
        uint32_t    integer_num;
        float       float_num;
    };
} def_argv_t;

typedef struct unit_item_argv
{
    union {
        struct {
            def_argv_t  argv[6];
        } def;
    } u;

} unit_item_argv_t;

typedef struct unit_item_desc
{
//    char        *pItem_name;

    /**
     *  @brief  Authenticate the opcode is supported or not
     *
     *  @param [in] opcode          opcode, @ref aip_opcode_t
     *  @return
     *      UNIT_ITEM_ERR_GET_UITEM: support this opcode,
     *      otherwise: Not support
     */
    unit_item_err_t     (*cb_auth_opcode)(aip_opcode_t opcode, unit_item_argv_t *pArgv);

    unit_item_err_t     (*cb_configuate)(unit_item_cfg_t *pCfg);
    unit_item_err_t     (*cb_proc)(unit_item_cfg_t *pCfg);
    unit_item_err_t     (*cb_analyze_result)(unit_item_cfg_t *pCfg);
    unit_item_err_t     (*cb_log_data)(unit_item_cfg_t *pCfg);

    /**
     *  @brief Display usage
     *
     *  @return
     *      enunm unit_item_err
     */
    unit_item_err_t     (*cb_usage)();

    void                (*cb_delay)();

    char        *pDescription;

} unit_item_desc_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================

//=============================================================================
//                  Public Function Definition
//=============================================================================

#ifdef __cplusplus
}
#endif

#endif
