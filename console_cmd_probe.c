/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file console_cmd_probe.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/04
 * @license
 * @description
 */


#include <stdlib.h>
#include <string.h>

#if defined(CONFIG_SIM)
#include <windows.h>
#include "fake_api.h"

#else
#include "console_cmds.h"
#include "console.h"
#include "main.h"
#include "log.h"

#include "ads1256.h"
#include "dac8562.h"
#include "74HC4051.h"
#endif

#include "probe.h"
#include "aip_package.h"
#include "unit_item.h"
//=============================================================================
//                  Constant Definition
//=============================================================================
#define CONFIG_PROBE_TIMEOUT_MS         HAL_MAX_DELAY /* ms */


#define CRC16POLY1                      0x1021U
#define CRC16POLY2                      0x8408U  /* left-right reversal */

typedef enum dut_id
{
    DUT_ID_1    = 0,
    DUT_ID_2,
    DUT_ID_3,
    DUT_ID_4,
    DUT_ID_HOST,
    DUT_ID_TOTAL,
} dut_id_t;


//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct dut_attr
{
    char            *name;
    uint8_t         i2c_addr;
    uint8_t         has_exist;
} dut_attr_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================
I2C_HandleTypeDef   g_hI2C2;
extern multiplexer_handle_t    g_HMult;

static unit_item_argv_t     g_uitem_argv = {0};
static uint32_t             g_buf[CONFIG_XFER_MAX_LENGTH >> 2] = {0};
static dut_attr_t           g_dut_attr[DUT_ID_TOTAL] =
{
    [DUT_ID_1] = { .name = "DUT 1", .i2c_addr = CONFIG_DUT_1_I2C_ADDRESS, .has_exist = false, },
    [DUT_ID_2] = { .name = "DUT 2", .i2c_addr = CONFIG_DUT_2_I2C_ADDRESS, .has_exist = false, },
    [DUT_ID_3] = { .name = "DUT 3", .i2c_addr = CONFIG_DUT_3_I2C_ADDRESS, .has_exist = false, },
    [DUT_ID_4] = { .name = "DUT 4", .i2c_addr = CONFIG_DUT_4_I2C_ADDRESS, .has_exist = false, },
};

static uint32_t     g_dut_flags = 0;
static uint32_t     g_code_start[DAC8562_CHANNEL_ALL] = {0};
static uint32_t     g_code_increase[DAC8562_CHANNEL_ALL] = {0};
static uint8_t      g_dac_channel = 0;
static int          g_keep_slot_size = -1;
static int          g_cur_slot = 0;
static aip_opcode_t  g_opcode = AIP_OPCODE_NONE;

extern unit_item_desc_t        g_uitem_opa;
extern unit_item_desc_t        g_uitem_dac8562;
extern unit_item_desc_t        g_uitem_adc;

static unit_item_desc_t     *g_pUitems[] =
{
    &g_uitem_opa,
    &g_uitem_dac8562,
    &g_uitem_adc,
    0
};

static unit_item_result_t       g_uitem_results[CONFIG_UNIT_ITEM_RESULT_MAX] = {0};

//=============================================================================
//                  Private Function Definition
//=============================================================================
#if defined(CONFIG_SIM)
#include "fake_api.h"

#define CONFIG_DAC_MAX_STEP             65535

#define probe_init                  probe_mst_init
#define probe_deinit                probe_mst_deinit
#define probe_read                  probe_mst_read
#define probe_write                 probe_mst_write

static probe_phy_t      g_probe_phy = {};

#else

static probe_err_t
_probe_init(void)
{
    probe_err_t     rval = PROBE_ERR_OK;

    g_hI2C2.Instance             = I2C2;
    g_hI2C2.Init.ClockSpeed      = 100000; // 100K
    g_hI2C2.Init.DutyCycle       = I2C_DUTYCYCLE_2;
    g_hI2C2.Init.OwnAddress1     = 0;
    g_hI2C2.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
    g_hI2C2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    g_hI2C2.Init.OwnAddress2     = 0;
    g_hI2C2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    g_hI2C2.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

    if( HAL_I2C_Init(&g_hI2C2) != HAL_OK )
    {
        rval = PROBE_ERR_PHY_INIT_FAIL;
    }
    return rval;
}

static probe_err_t
_probe_deinit(void)
{
    probe_err_t     rval = PROBE_ERR_OK;

    HAL_I2C_DeInit(&g_hI2C2);
    return rval;
}

static probe_err_t
_probe_read(uint8_t *pData, uint32_t *pLength, void *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;
    uint32_t        dut_id = *(uint32_t*)pExtra;
    uint16_t        data_len = (uint16_t)(*pLength);

    if( HAL_I2C_Master_Receive(&g_hI2C2, (uint16_t)g_dut_attr[dut_id].i2c_addr,
                                 (uint8_t*)pData, data_len, CONFIG_PROBE_TIMEOUT_MS) != HAL_OK )
    {
        rval = PROBE_ERR_PHY_READ_FAIL;
    }

    while( HAL_I2C_GetState(&g_hI2C2) != HAL_I2C_STATE_READY ) {}

    *pLength = (uint32_t)(g_hI2C2.pBuffPtr - pData);

    // printf("0x%08X, 0x%08X\n", (uint32_t)g_hI2C2.pBuffPtr, (uint32_t)pData);

    return rval;
}

static probe_err_t
_probe_write(uint8_t *pData, uint32_t length, void *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;
    uint32_t        dut_id = *(uint32_t*)pExtra;

    if( HAL_I2C_Master_Transmit(&g_hI2C2, (uint16_t)g_dut_attr[dut_id].i2c_addr,
                                (uint8_t*)pData, length, CONFIG_PROBE_TIMEOUT_MS) != HAL_OK )
    {
        rval = PROBE_ERR_PHY_WRITE_FAIL;
    }

    while( HAL_I2C_GetState(&g_hI2C2) != HAL_I2C_STATE_READY ) {}

    return rval;
}

static probe_phy_t      g_probe_phy =
{
    .cb_init   = _probe_init,
    .cb_deinit = _probe_deinit,
    .cb_read   = _probe_read,
    .cb_write  = _probe_write,
};
#endif // defined

static void
_probe_usage()
{
    unit_item_desc_t    **ppCur = (unit_item_desc_t**)g_pUitems;

    while( *ppCur )
    {
        unit_item_desc_t    *pCur = *ppCur;
        if( pCur->cb_usage )
            pCur->cb_usage();

        ppCur++;
    }
    return;
}

static int
_probe_get_params(int argc, char **argv)
{
    if (argc < 2) {
        return -1;
    }

    argv++; argc--;
    while(argc) {
        if (!strcmp(argv[0], "--dacAStart")) {
            /* start code value of DAC8562 */
            argv++; argc--;
            g_code_start[DAC8562_CHANNEL_A] = (*(argv[0] + 1) == 'x')
                                            ? strtol(argv[0] + 2, NULL, 16)
                                            : strtol(argv[0], NULL, 10);

            g_dac_channel |= (0x1 << DAC8562_CHANNEL_A);
        } else if (!strcmp(argv[0], "--dacBStart")) {
            /* start code value of DAC8562 */
            argv++; argc--;
            g_code_start[DAC8562_CHANNEL_B] = (*(argv[0] + 1) == 'x')
                                            ? strtol(argv[0] + 2, NULL, 16)
                                            : strtol(argv[0], NULL, 10);

            g_dac_channel |= (0x1 << DAC8562_CHANNEL_B);
        } else if (!strcmp(argv[0], "--dacAGap")) {
            /* code increase of DAC8562 */
            argv++; argc--;
            g_code_increase[DAC8562_CHANNEL_A] = (*(argv[0] + 1) == 'x')
                                            ? strtol(argv[0] + 2, NULL, 16)
                                            : strtol(argv[0], NULL, 10);
        } else if (!strcmp(argv[0], "--dacBGap")) {
            /* code increase of DAC8562 */
            argv++; argc--;
            g_code_increase[DAC8562_CHANNEL_B] = (*(argv[0] + 1) == 'x')
                                            ? strtol(argv[0] + 2, NULL, 16)
                                            : strtol(argv[0], NULL, 10);
        } else if (!strcmp(argv[0], "--dut")) {
            /* Device flag */
            argv++; argc--;
            for(int i = 0; i < strlen(argv[0]); i++)
            {
                if( argv[0][i] == '1' )
                    g_dut_flags |= (0x1u << DUT_ID_1);
                else if( argv[0][i] == '2' )
                    g_dut_flags |= (0x1u << DUT_ID_2);
                else if( argv[0][i] == '3' )
                    g_dut_flags |= (0x1u << DUT_ID_3);
                else if( argv[0][i] == '4' )
                    g_dut_flags |= (0x1u << DUT_ID_4);
                else if( argv[0][i] == '0' )
                    g_dut_flags |= (0x1u << DUT_ID_HOST);
            }
        } else if (!strcmp(argv[0], "--keepMsg")) {
            argv++; argc--;
            g_keep_slot_size = (*(argv[0] + 1) == 'x')
                             ? strtol(argv[0] + 2, NULL, 16)
                             : strtol(argv[0], NULL, 10);
        } else if (!strcmp(argv[0], "--opcode")) {
            /* opcode */
            argv++; argc--;
            g_opcode = (*(argv[0] + 1) == 'x')
                     ? (aip_opcode_t)strtol(argv[0] + 2, NULL, 16)
                     : (aip_opcode_t)strtol(argv[0], NULL, 10);
        } else if (!strcmp(argv[0], "--arg1")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[0].pArgv = argv[0];
        } else if (!strcmp(argv[0], "--arg2")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[1].pArgv = argv[0];
        } else if (!strcmp(argv[0], "--arg3")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[2].pArgv = argv[0];
        } else if (!strcmp(argv[0], "--arg4")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[3].pArgv = argv[0];
        } else if (!strcmp(argv[0], "--arg5")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[4].pArgv = argv[0];
        } else if (!strcmp(argv[0], "--arg6")) {
            /* argv */
            argv++; argc--;
            g_uitem_argv.u.def.argv[5].pArgv = argv[0];
        }
        else {

        }
        argv++; argc--;
    }

    return 0;
}

static int
_probe_init_input_capture()
{
    return 0;
}


int
_console_cmd_probe(
    int                 argc,
    char                **argv,
    void                *pExtra)
{
    int         rval = 0;

    if( !strncasecmp(argv[1], "h", strlen("h")) ||
        !strncasecmp(argv[1], "-h", strlen("-h")) ||
        !strncasecmp(argv[1], "?", strlen("?")) )
    {
        _probe_usage();
        return rval;
    }

    g_code_start[DAC8562_CHANNEL_A] = 0;
    g_code_start[DAC8562_CHANNEL_B] = 0;
    g_code_increase[DAC8562_CHANNEL_A] = 0;
    g_code_increase[DAC8562_CHANNEL_B] = 0;

    if( !strncasecmp(argv[1], "reset", strlen("reset")) )
    {
        g_keep_slot_size = -1;
        g_cur_slot       = 0;
        memset(g_uitem_results, 0xFF, sizeof(g_uitem_results));
        return rval;
    }


#if 1 // debug
    if( !strncmp(argv[1], "x", 1)  )
    {
        g_code_start[DAC8562_CHANNEL_A]    = 0;
        g_code_increase[DAC8562_CHANNEL_A] = 0;

        g_dut_flags |= (0x1 << DUT_ID_1);

        g_opcode = AIP_OPCODE_TEST_OPA1_FOLLOWER_DAC;

        g_uitem_argv.u.def.argv[0].pArgv = "32";
        g_uitem_argv.u.def.argv[1].pArgv = "1";
    }
    else
#endif
    {
        _probe_get_params(argc, argv);
    }

    if( (g_code_start[DAC8562_CHANNEL_A] & ~0xFFFF) || (g_code_increase[DAC8562_CHANNEL_A] & ~0xFFFF) ||
        (g_code_start[DAC8562_CHANNEL_B] & ~0xFFFF) || (g_code_increase[DAC8562_CHANNEL_B] & ~0xFFFF) ||
        g_keep_slot_size > CONFIG_UNIT_ITEM_RESULT_MAX )
    {
        err("wrong parameters, "
            "DAC configuration (< 65535) or keep message (< %d) out range\n",
            CONFIG_UNIT_ITEM_RESULT_MAX);
        return -1;
    }

    do { /* start probing */
        probe_err_t         rst = PROBE_ERR_OK;
        unit_item_desc_t    *pUitem_act = 0;

        _probe_init_input_capture();

        rval = dac8562_init();
        if( rval )  break;

        rst = probe_init(&g_probe_phy);
        if( rst )
        {
            err("probe phy init fail \n");
            rval = PROBE_ERR_PHY_INIT_FAIL;
            break;
        }

        /**
         *  Setup Remote DUT
         */
        for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
        {
            probe_cmd_t     cmd = PROBE_CMD_NONE;
            uint8_t         *pCur = (uint8_t*)&g_buf;
            uint32_t        buf_len = sizeof(g_buf);

            if( !(g_dut_flags & (0x1u << dut_id)) )
                continue;

            if( dut_id == DUT_ID_HOST )
            {
                g_dut_attr[DUT_ID_HOST].has_exist = true;
                break;
            }

            // ping DUT
            g_buf[0] = FOURCC('d', 'u', 't', g_dut_attr[dut_id].i2c_addr);
            rst = probe_write(PROBE_CMD_PING, (uint8_t*)&g_buf, sizeof(uint32_t), (void*)&dut_id);
            if( rst != PROBE_ERR_OK )
            {
                err("send 'ping' fail \n");
                continue;
            }

            memset(g_buf, 0x0, sizeof(g_buf));
            buf_len = 12;
            rst = probe_read(&cmd, pCur, &buf_len, (void*)&dut_id);
            if( rst != PROBE_ERR_OK )
            {
                err("No %s\n", g_dut_attr[dut_id].name);
                continue;
            }
            g_dut_attr[dut_id].has_exist = true;

            msg("dut_id (0x%X) exist, resp cmd= x%X \n", dut_id + 1, cmd);
        }

        {   /* authenticate opcode (opcode from user) */
            unit_item_desc_t    **ppCur = (unit_item_desc_t**)g_pUitems;
            while( *ppCur )
            {
                unit_item_desc_t    *pCur = *ppCur;
                if( pCur->cb_auth_opcode &&
                    pCur->cb_auth_opcode(g_opcode, &g_uitem_argv) == UNIT_ITEM_ERR_GET_UITEM )
                {
                    pUitem_act = pCur;
                    break;
                }

                ppCur++;
            }
        }

        if( !pUitem_act )
        {
            err("No unit item \n");
            rval = -1;
            break;
        }

        for(int stage_a = g_code_start[DAC8562_CHANNEL_A]; stage_a < CONFIG_DAC_MAX_STEP; stage_a += g_code_increase[DAC8562_CHANNEL_A])
        {
            for(int stage_b = g_code_start[DAC8562_CHANNEL_B]; stage_b < CONFIG_DAC_MAX_STEP; stage_b += g_code_increase[DAC8562_CHANNEL_B])
            {
                unit_item_err_t     err_rval = UNIT_ITEM_ERR_OK;
                float               voltage = 0.0f;

                /**
                 *  Configure DUTs
                 */
                for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
                {
                    unit_item_cfg_t     cfg = { .opcode = g_opcode, };

                    if( g_dut_attr[dut_id].has_exist == false )
                        continue;

                    // switch multiplexer for ADS1256
                    if( g_opcode >= AIP_OPCODE_TEST_OPA2_CALIBRATION &&
                        g_opcode <= AIP_OPCODE_TEST_OPA_CASCADING )
                    {
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y4); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y5); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y6); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y7); break;
                            default:    break;
                        }
                    }
                    else
                    {
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y0); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y1); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y2); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y3); break;
                            default:    break;
                        }
                    }

                    memset(g_buf, 0x0, sizeof(g_buf));

                    cfg.dut_id        = dut_id;
                    cfg.pBuf_pool     = (uint8_t*)&g_buf;
                    cfg.buf_pool_size = sizeof(g_buf);
                    cfg.dac_chA_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_A)) ? stage_a : -1;
                    cfg.dac_chB_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_B)) ? stage_b : -1;

                    if( pUitem_act->cb_configuate )
                    {
                        err_rval = pUitem_act->cb_configuate(&cfg);
                        if( err_rval != UNIT_ITEM_ERR_OK )
                        {
                            err("OpCode (0x%X): err_code= 0x%X\n", cfg.opcode, err_rval);
                        }
                    }
                }

                /**
                 *  DUTs verification process
                 */
                for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
                {
                    unit_item_cfg_t     cfg = { .opcode = g_opcode, };

                    if( g_dut_attr[dut_id].has_exist == false )
                        continue;

                    // switch multiplexer for ADS1256
                    if( g_opcode >= AIP_OPCODE_TEST_OPA2_CALIBRATION &&
                        g_opcode <= AIP_OPCODE_TEST_OPA_CASCADING )
                    {
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y4); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y5); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y6); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y7); break;
                            default:    break;
                        }
                    }
                    else
                    {
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y0); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y1); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y2); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y3); break;
                            default:    break;
                        }
                    }

                    memset(g_buf, 0x0, sizeof(g_buf));

                    cfg.dut_id        = dut_id;
                    cfg.pBuf_pool     = (uint8_t*)&g_buf;
                    cfg.buf_pool_size = sizeof(g_buf);
                    cfg.dac_chA_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_A)) ? stage_a : -1;
                    cfg.dac_chB_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_B)) ? stage_b : -1;

                    if( pUitem_act->cb_proc )
                    {
                        err_rval = pUitem_act->cb_proc(&cfg);
                        if( err_rval != UNIT_ITEM_ERR_OK )
                        {
                            err("OpCode (0x%X): err_code= 0x%X\n", cfg.opcode, err_rval);
                        }
                    }
                }

                if( pUitem_act->cb_delay )
                    pUitem_act->cb_delay();
                else
                    HAL_Delay(200);

                /**
                 *  Get report of DUTs
                 */
                for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
                {
                    unit_item_cfg_t     cfg = { .opcode = g_opcode, };

                    if( g_dut_attr[dut_id].has_exist == false )
                        continue;

                    // Get Vcap from DUT
                    switch( dut_id )
                    {
                        case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y0); break;
                        case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y1); break;
                        case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y2); break;
                        case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y3); break;
                        default:    break;
                    }

                    voltage = ads1256_average_voltage(CONFIG_ADS1256_VCAP_AIN, CONIFG_ADS1256_SAMPLE_TIMES, 0);
                    if( g_keep_slot_size == -1 )
                        msg("@DUT #%d Vcap %2.6f, ", dut_id + 1, voltage);

                    // switch multiplexer for ADS1256
                    if( g_opcode >= AIP_OPCODE_TEST_OPA2_CALIBRATION &&
                        g_opcode <= AIP_OPCODE_TEST_OPA_CASCADING )
                    {
                        // OPA2 output
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y4); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y5); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y6); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y7); break;
                            default:    break;
                        }
                    }
                    else
                    {
                        switch( dut_id )
                        {
                            case DUT_ID_1:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y0); break;
                            case DUT_ID_2:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y1); break;
                            case DUT_ID_3:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y2); break;
                            case DUT_ID_4:  multiplexer_set_channel(&g_HMult, MULTIPLEXER_CHNNL_Y3); break;
                            default:    break;
                        }
                    }

                    memset(g_buf, 0x0, sizeof(g_buf));

                    cfg.dut_id        = dut_id;
                    cfg.pBuf_pool     = (uint8_t*)&g_buf;
                    cfg.buf_pool_size = sizeof(g_buf);
                    cfg.vcap          = voltage;
                    cfg.dac_chA_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_A)) ? stage_a : -1;
                    cfg.dac_chB_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_B)) ? stage_b : -1;

                    cfg.pUItem_results      = (g_keep_slot_size == -1) ? 0 : (unit_item_result_t*)&g_uitem_results;
                    cfg.uitem_rst_cnt       = g_cur_slot;
                    cfg.uitem_rst_slot_size = g_keep_slot_size;

                    if( pUitem_act->cb_analyze_result )
                    {
                        err_rval = pUitem_act->cb_analyze_result(&cfg);
                        if( err_rval != UNIT_ITEM_ERR_OK )
                        {
                            err("OpCode (0x%X): err_code= 0x%X\n", cfg.opcode, err_rval);
                        }

                        g_cur_slot = cfg.uitem_rst_cnt;
                    }
                }


                /**
                 *  log results
                 */
                if( g_keep_slot_size != -1 )
                {
                    for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
                    {
                        unit_item_cfg_t     cfg = { .opcode = g_opcode, };

                        if( g_dut_attr[dut_id].has_exist == false )
                            continue;

                        cfg.dut_id        = dut_id;
                        cfg.vcap          = voltage;
                        cfg.dac_chA_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_A)) ? stage_a : -1;
                        cfg.dac_chB_stage = (g_dac_channel & (0x1 << DAC8562_CHANNEL_B)) ? stage_b : -1;

                        cfg.pUItem_results      = (unit_item_result_t*)&g_uitem_results;
                        cfg.uitem_rst_cnt       = g_cur_slot;
                        cfg.uitem_rst_slot_size = g_keep_slot_size;

                        if( pUitem_act->cb_log_data )
                        {
                            err_rval = pUitem_act->cb_log_data(&cfg);
                            if( err_rval != UNIT_ITEM_ERR_OK )
                            {
                                err("log result fail: err_code= 0x%X\n", err_rval);
                            }

                            g_cur_slot = cfg.uitem_rst_cnt;
                        }
                    }
                }

                if( g_code_increase[DAC8562_CHANNEL_B] == 0 )
                    break; /* only do once */
            }

            if( g_code_increase[DAC8562_CHANNEL_A] == 0 )
                break; /* only do once */
        }

    } while(0);

    probe_deinit();

    return rval;
}

#if defined(CONFIG_SIM)
#else

static int
_console_cmd_ping(
    int                 argc,
    char                **argv,
    void                *pExtra)
{
    int         rval = 0;
    uint32_t    dut_flags = 0;

    if( argc < 2 )
    {
        err("too less parameters\n");
        return -1;
    }

    if( probe_init(&g_probe_phy) != PROBE_ERR_OK )
    {
        err("probe phy init fail \n");
        return -1;
    }

    for(int i = 0; i < strlen(argv[1]); i++)
    {
        if( argv[1][i] == '1' )
            dut_flags |= (0x1u << DUT_ID_1);
        else if( argv[1][i] == '2' )
            dut_flags |= (0x1u << DUT_ID_2);
        else if( argv[1][i] == '3' )
            dut_flags |= (0x1u << DUT_ID_3);
        else if( argv[1][i] == '4' )
            dut_flags |= (0x1u << DUT_ID_4);
    }

    // I2C communicate with DUT
    for(int dut_id = DUT_ID_1; dut_id < DUT_ID_TOTAL; dut_id++)
    {
        probe_err_t     rst = PROBE_ERR_OK;
        probe_cmd_t     cmd = PROBE_CMD_NONE;
        uint8_t         *pCur = (uint8_t*)&g_buf;
        uint32_t        buf_len = sizeof(g_buf);

        if( !(dut_flags & (0x1u << dut_id)) )
            continue;

        // ping DUT
        g_buf[0] = FOURCC('d', 'u', 't', g_dut_attr[dut_id].i2c_addr);
        rst = probe_write(PROBE_CMD_PING, (uint8_t*)&g_buf, sizeof(uint32_t), (void*)&dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("send 'ping' fail (err= 0x%X)\n", rst);
            continue;
        }

        memset(g_buf, 0x0, sizeof(g_buf));
        buf_len = 12;
        rst = probe_read(&cmd, pCur, &buf_len, (void*)&dut_id);
        if( rst != PROBE_ERR_OK )
        {
            err("No %s (err= %d)\n", g_dut_attr[dut_id].name, rst);
            continue;
        }

        if( cmd == PROBE_CMD_ACK )
            printf("%s ack\n", g_dut_attr[dut_id].name);

    }

    return rval;
}

static console_cmd_t    const g_console_cmd_desc_probe =
{
    .pCmd_name    = "probe",
    .cmd_exec     = &_console_cmd_probe,
    .pDescription = "  probe analog IP of DUT\n"
                    "  usage: probe [option] [value] ...\n"
                    "    help to get opcode\n\n"
                    "    --dut [Device ID flag]\n"
                    "       0: HOST\n"
                    "       1: DUT 1\n"
                    "       2: DUT 2\n"
                    "       3: DUT 3\n"
                    "       4: DUT 4\n\n"
                    "    --dacAStart, --dacBStart [Start input code]\n"
                    "       input code of DAC (0 ~ 65535)\n\n"
                    "    --dacAGap, --dacBGap [Increased code]\n"
                    "       unit of auto-increasing\n"
                    "       ps. '0' means to probe once\n\n"
                    "    --keepMsg [number]\n"
                    "       Log messages when probing times is [number]\n"
                    "    --arg1 ~ --arg6\n"
                    "       paraments 1 ~ 6\n\n"
                    "    e.g. probe --dut 1 --dacAStart 32768 --dacAGap 0 --opcode 0xC000 --arg1 10\n"
                    "         probe --dut 1 --dacAStart 32768 --dacAGap 0 --dacBStart 32768 --dacBGap 0 --opcode 0xC000\n\n",
};

static console_cmd_t    const g_console_cmd_desc_ping =
{
    .pCmd_name    = "ping",
    .cmd_exec     = &_console_cmd_ping,
    .pDescription = "  ping DUT\n"
                    "  usage: ping [DUT ID flag]\n"
                    "    [DUT ID flag]\n"
                    "       1: DUT 1\n"
                    "       2: DUT 2\n"
                    "       3: DUT 3\n"
                    "       4: DUT 4\n"
                    "    e.g. ping 1234\n\n",
};
//=============================================================================
//                  Public Function Definition
//=============================================================================
int register_cmd_probe(void)
{
    console_err_t   err_code = CONSOLE_ERR_OK;
    err_code = console_register_cmd((console_cmd_t*)&g_console_cmd_desc_probe);
    return (int)err_code;
}

int register_cmd_ping(void)
{
    console_err_t   err_code = CONSOLE_ERR_OK;
    err_code = console_register_cmd((console_cmd_t*)&g_console_cmd_desc_ping);
    return (int)err_code;
}
#endif
