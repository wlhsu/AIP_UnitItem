/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file probe.c
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/04
 * @license
 * @description
 */

#include <stdio.h>
#include <string.h>
#include "probe.h"

#include "aip_package.h"
#include <windows.h>
//=============================================================================
//                  Constant Definition
//=============================================================================
#define MSG_MST_TX          (WM_USER + 100)
#define MSG_MST_RX          (WM_USER + 200)
//=============================================================================
//                  Macro Definition
//=============================================================================
#define err(str, ...)       do{ printf("[%s:%d]" str, __func__, __LINE__, ##__VA_ARGS__); while(1); }while(0)

#define trace(str, ...)     do { fprintf(stdout, "[%s:%d] " str, __func__, __LINE__,  ##__VA_ARGS__); fflush(stdout); } while(0)

//=============================================================================
//                  Structure Definition
//=============================================================================
typedef struct probe_hdr
{
    uint8_t     cmd;
    uint8_t     length;
    uint16_t    crc;

} probe_hdr_t;

typedef struct my_msg_info
{
    uint8_t     *pMsg_buf;
    int         msg_len;
} my_msg_info_t;
//=============================================================================
//                  Global Data Definition
//=============================================================================
#if 0
static HANDLE               g_hEvent_mst_tx;
static HANDLE               g_hEvent_slv_tx;
static HANDLE               g_hEvent_mst_rx;
static HANDLE               g_hEvent_slv_rx;

static HANDLE               g_Mutex_mst_tx;
static HANDLE               g_Mutex_mst_rx;
#endif

static my_msg_info_t           g_msg_mst_tx;
static my_msg_info_t           g_msg_mst_rx;

static uint32_t              g_mst_tx_buf[1024 >> 2] = {0};
static uint32_t              g_mst_rx_buf[1024 >> 4] = {0};

//            WaitForSingleObject(g_Mutex_mst_tx, INFINITE);
//            ReleaseMutex(g_Mutex_mst_tx);
//
//            WaitForSingleObject(g_Mutex_mst_rx, INFINITE);
//            ReleaseMutex(g_Mutex_mst_rx);


extern DWORD     g_thid_dut;
extern DWORD     g_thid_host;
//=============================================================================
//                  Private Function Definition
//=============================================================================
static int
_setup_event()
{
#if 0
    if( g_Mutex_mst_tx == NULL )
    {
        g_Mutex_mst_tx = CreateMutex(NULL,  // default security attributes
                              FALSE, // initially not owned
                              NULL); // unnamed mutex
        if( g_Mutex_mst_tx == NULL )
        {
            printf("CreateMutex() error %u\n", (unsigned int)GetLastError());
        }
    }

    if( g_Mutex_mst_rx == NULL )
    {
        g_Mutex_mst_rx = CreateMutex(NULL,  // default security attributes
                              FALSE, // initially not owned
                              NULL); // unnamed mutex
        if( g_Mutex_mst_rx == NULL )
        {
            printf("CreateMutex() error %u\n", (unsigned int)GetLastError());
        }
    }


    if( g_hEvent_mst_tx == NULL )
    {
        g_hEvent_mst_tx = CreateEvent(NULL,               // default security attributes
                                       TRUE,               // manual-reset event
                                       TRUE,              // initial state is nonsignaled
                                       TEXT("WriteEvent")  // object name
                                       );
        if( g_hEvent_mst_tx == NULL )
        {
            err("CreateEvent failed (%u)\n", (unsigned)GetLastError());;
        }

        ResetEvent(g_hEvent_mst_tx);
    }


    if( g_hEvent_slv_tx == NULL )
    {
        g_hEvent_slv_tx = CreateEvent(NULL,               // default security attributes
                                       TRUE,               // manual-reset event
                                       TRUE,              // initial state is nonsignaled
                                       TEXT("WriteEvent")  // object name
                                       );
        if( g_hEvent_slv_tx == NULL )
        {
            err("CreateEvent failed (%u)\n", (unsigned)GetLastError());
        }

        ResetEvent(g_hEvent_slv_tx);
    }


    if( g_hEvent_mst_rx == NULL )
    {
        g_hEvent_mst_rx = CreateEvent(NULL,               // default security attributes
                                       TRUE,               // manual-reset event
                                       TRUE,              // initial state is nonsignaled
                                       TEXT("WriteEvent")  // object name
                                       );
        if( g_hEvent_mst_rx == NULL )
        {
            err("CreateEvent failed (%u)\n", (unsigned)GetLastError());
        }

        ResetEvent(g_hEvent_mst_rx);
    }

    if( g_hEvent_slv_rx == NULL )
    {
        g_hEvent_slv_rx = CreateEvent(NULL,               // default security attributes
                                       TRUE,               // manual-reset event
                                       TRUE,              // initial state is nonsignaled
                                       TEXT("WriteEvent")  // object name
                                       );
        if( g_hEvent_slv_rx == NULL )
        {
            err("CreateEvent failed (%u)\n", (unsigned)GetLastError());
        }

        ResetEvent(g_hEvent_slv_rx);
    }
#endif
    return 0;
}

static int
_terminate()
{
#if 0
    if( g_hEvent_mst_tx )
    {
        CloseHandle(g_hEvent_mst_tx);
        g_hEvent_mst_tx = NULL;
    }

    if( g_hEvent_mst_rx )
    {
        CloseHandle(g_hEvent_mst_rx);
        g_hEvent_mst_rx = NULL;
    }

    if( g_hEvent_slv_tx )
    {
        CloseHandle(g_hEvent_slv_tx);
        g_hEvent_slv_tx = NULL;
    }

    if( g_hEvent_slv_rx )
    {
        CloseHandle(g_hEvent_slv_rx);
        g_hEvent_slv_rx = NULL;
    }
#endif
    return 0;
}
//=============================================================================
//                  Public Function Definition
//=============================================================================
probe_err_t
probe_mst_init(probe_phy_t *pPhy)
{
    int     rval = 0;

    _setup_event();

    return (rval) ? PROBE_ERR_UNKNOWN_FAIL : PROBE_ERR_OK;
}

probe_err_t
probe_mst_deinit(void)
{
    probe_err_t     rval = PROBE_ERR_OK;

    _terminate();
    return rval;
}

probe_err_t
probe_mst_read(
    probe_cmd_t *pCmd,
    uint8_t     *pData,
    uint32_t    *pLength,
    void        *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;

    do {
        uint32_t        len = 1024;
        probe_hdr_t     *pHdr = 0;

        MSG     msg;
        while(true)
        {
            if( !GetMessage(&msg, 0, 0, 0) ) //get msg from message queue
            {
                Sleep(1);
                continue;
            }

            switch( msg.message )
            {
                case MSG_MST_RX:
                    {
                        my_msg_info_t  *pMsg_info = (my_msg_info_t*)msg.wParam;

                        pHdr = (probe_hdr_t*)pMsg_info->pMsg_buf;

                        if( pCmd )  *pCmd = (probe_cmd_t)pHdr->cmd;

                        len = *pLength;
                        *pLength = 0;

                        memcpy(pData, (void*)(pHdr + 1), (pHdr->length < len) ? pHdr->length : len);
                        *pLength = pHdr->length;
                    }
                    return rval;

                default:
                    break;
            }
        }

    } while(0);

    return rval;
}

probe_err_t
probe_mst_write(
    probe_cmd_t cmd,
    uint8_t     *pData,
    uint32_t    length,
    void        *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;
    do {
        probe_hdr_t     *pHdr = 0;

        memset((void*)&g_mst_tx_buf, 0x0, sizeof(g_mst_tx_buf));

        pHdr = (probe_hdr_t*)&g_mst_tx_buf;
        pHdr->cmd    = cmd;
        pHdr->length = length;

        memcpy((void*)(pHdr + 1), pData, length);

        g_msg_mst_tx.pMsg_buf = (uint8_t*)&g_mst_tx_buf;
        g_msg_mst_tx.msg_len  = length + sizeof(probe_hdr_t);
        if( !PostThreadMessage(g_thid_dut, MSG_MST_TX, (WPARAM)&g_msg_mst_tx, 0) ) //post thread msg
        {
            // fail
        }

    } while(0);
    return rval;
}

probe_err_t
probe_slv_init(probe_phy_t *pPhy)
{
    probe_err_t     rval = PROBE_ERR_OK;

    _setup_event();

    return rval;
}

probe_err_t
probe_slv_deinit(void)
{
    probe_err_t     rval = PROBE_ERR_OK;

    _terminate();
    return rval;
}

probe_err_t
probe_slv_read(
    probe_cmd_t *pCmd,
    uint8_t     *pData,
    uint32_t    *pLength,
    void        *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;

    do {
        uint32_t        len = 1024;
        probe_hdr_t     *pHdr = 0;

        MSG     msg;
        while(true)
        {
            if( !GetMessage(&msg, 0, 0, 0) ) //get msg from message queue
            {
                Sleep(1);
                continue;
            }

            switch( msg.message )
            {
                case MSG_MST_TX:
                    {
                        my_msg_info_t  *pMsg_info = (my_msg_info_t*)msg.wParam;

                        pHdr = (probe_hdr_t*)pMsg_info->pMsg_buf;

                        if( pCmd )  *pCmd = (probe_cmd_t)pHdr->cmd;

                        len = *pLength;
                        *pLength = 0;

                        memcpy(pData, (void*)(pHdr + 1), (pHdr->length < len) ? pHdr->length : len);
                        *pLength = pHdr->length;
                    }
                    return rval;

                default:
                    break;
            }
        }
    } while(0);

    return rval;
}

probe_err_t
probe_slv_write(
    probe_cmd_t cmd,
    uint8_t     *pData,
    uint32_t    length,
    void        *pExtra)
{
    probe_err_t     rval = PROBE_ERR_OK;
    do {
        probe_hdr_t     *pHdr = 0;

        memset((void*)&g_mst_rx_buf, 0x0, sizeof(g_mst_rx_buf));

        pHdr = (probe_hdr_t*)&g_mst_rx_buf;
        pHdr->cmd    = cmd;
        pHdr->length = length;

        memcpy((void*)(pHdr + 1), pData, length);

        g_msg_mst_rx.pMsg_buf = (uint8_t*)&g_mst_rx_buf;
        g_msg_mst_rx.msg_len  = length + sizeof(probe_hdr_t);
        if( !PostThreadMessage(g_thid_host, MSG_MST_RX, (WPARAM)&g_msg_mst_rx, 0) ) //post thread msg
        {
            // fail
        }

    } while(0);
    return rval;
}


