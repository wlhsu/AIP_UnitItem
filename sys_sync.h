/**
 * Copyright (c) 2022 Wei-Lun Hsu. All Rights Reserved.
 */
/** @file sys_sync.h
 *
 * @author Wei-Lun Hsu
 * @version 0.1
 * @date 2022/01/13
 * @license
 * @description
 */

#ifndef __sys_sync_H_w7urF5uL_lDz6_H5DM_sW7J_uHspqEMKvPUt__
#define __sys_sync_H_w7urF5uL_lDz6_H5DM_sW7J_uHspqEMKvPUt__

#ifdef __cplusplus
extern "C" {
#endif


#include <stdbool.h>
#include <windows.h>
//=============================================================================
//                  Constant Definition
//=============================================================================

//=============================================================================
//                  Macro Definition
//=============================================================================

//=============================================================================
//                  Structure Definition
//=============================================================================

//=============================================================================
//                  Global Data Definition
//=============================================================================

//=============================================================================
//                  Private Function Definition
//=============================================================================
static bool     g_lock = true;
//=============================================================================
//                  Public Function Definition
//=============================================================================
static inline void __wait_event()
{
    bool    is_lock = true;

    while( is_lock == true )
    {
        is_lock = g_lock;

        __asm("nop");
        __asm("nop");
    }
    g_lock = true;

    return;
}

static inline void __trigger_event()
{
    g_lock = false;
    return;
}

#ifdef __cplusplus
}
#endif

#endif
